# LTM - python-tools for creating LTM-files for libTAS
This project is intended to help you create LTM-files by writing python scripts, which describe a TAS.

## Dependencies
This project only requires Python 3.x . No additional libraries are needed.

Can also be used with PyPy.

## Simple example
