def lazy_mouse(frames):
    last_mouse_position = (0, 0)
    for frame in frames:
        if frame.mouse_input.is_empty():
            (frame.mouse_input.mouse_x, frame.mouse_input.mouse_y) = last_mouse_position
        else:
            last_mouse_position = (
                frame.mouse_input.mouse_x, frame.mouse_input.mouse_y)
    return frames


class MouseInput:

    def __init__(self, mouse_x: int = 0, mouse_y: int = 0, buttons=[]):
        self.mouse_x = mouse_x
        self.mouse_y = mouse_y
        self.buttons = buttons

    def to_inputs(self):
        buttons_string = ''
        for i in range(1, 6):
            if i in self.buttons:
                buttons_string += str(i)
            else:
                buttons_string += '.'

        return '{0}:{1}:{2}'.format(int(self.mouse_x), int(self.mouse_y), buttons_string)

    def get_mouse_position(self):
        return (self.mouse_x, self.mouse_y)

    def is_empty(self):
        return self.mouse_x == 0 and self.mouse_y == 0 and len(self.buttons) == 0


class Frame:

    def __init__(self, frame: int, mouse_input: MouseInput, keys=[]):
        self.frame = frame
        self.mouse_input = mouse_input
        self.keys = keys

    def to_inputs(self):
        return '|{0}|{1}|'.format(':'.join(self.keys), self.mouse_input.to_inputs())

    def __str__(self):
        return self.to_inputs()

    def __mul__(self, other: int):
        result = []
        # TODO: should I clone the frames?
        for _ in range(0, other):
            result.append(self)
        return result

    def __add__(self, other):
        if self.frame != other.frame:
            raise Exception(
                'You can only add Frames with the same frame number. Tried to add frames {} and {}', self.frame, other.frame)

        mouse_input = self.mouse_input
        if mouse_input.is_empty():
            mouse_input = other.mouse_input
        return Frame(self.frame, mouse_input, self.keys + other.keys)
