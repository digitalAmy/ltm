from ltm.input_config import InputConfig
from ltm.annotations import Annotations
from ltm.inputs import Frame, MouseInput
from typing import List
import tarfile
import uuid
import os


class LtmFile:

    def __init__(self, input_config: InputConfig, annotations: Annotations, frames: List[Frame]):
        self.input_config = input_config
        self.annotations = annotations
        self.frames = frames

    def build_file(self, location: str):
        self.input_config.frame_count = str(len(self.frames))

        config_filename = '/tmp/' + str(uuid.uuid4())
        inputs_filename = '/tmp/' + str(uuid.uuid4())
        annotations_filename = '/tmp/' + str(uuid.uuid4())

        with open(config_filename, 'w') as config_file:
            config_file.write(self.input_config.fill_template())

        with open(inputs_filename, 'w') as inputs_file:
            inputs_file.write('\n'.join([str(frame) for frame in self.frames]))

        with open(annotations_filename, 'w') as annotations_file:
            annotations_file.write(self.annotations.content)

        with tarfile.open(location, 'w:gz') as targetTar:
            targetTar.add(config_filename, 'config.ini')
            targetTar.add(inputs_filename, 'inputs')
            targetTar.add(annotations_filename, 'annotations.txt')

        os.remove(config_filename)
        os.remove(inputs_filename)
        os.remove(annotations_filename)
