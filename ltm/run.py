from ltm.input_config import InputConfig
from ltm.inputs import Frame, MouseInput, lazy_mouse
from typing import List


class Step:

    def __init__(self, frame: int, keyboard_inputs: List[str], mouse_input: MouseInput = MouseInput(), hold: int = 1):
        self.frame = frame
        self.keyboard_inputs = keyboard_inputs
        self.mouse_input = mouse_input
        self.hold = hold

    def expand_to_frames(self):
        return Frame(self.frame, self.mouse_input, self.keyboard_inputs) * self.hold


class Run:

    def __init__(self, authors: List[str], framerate: int, rerecord_count_filename: str):
        self.steps = []
        self.authors = authors
        self.framerate = framerate
        self.rerecord_count_filename = rerecord_count_filename

    def add_step(self, step: Step):
        self.steps.append(step)

    def build_frames(self) -> List[Frame]:
        frames = []

        max_frame = 0
        for step in self.steps:
            max_frame = max(step.frame + step.hold - 1, max_frame)

        for i in range(0, max_frame + 1):
            frames.append(Frame(i, MouseInput()))

        for step in self.steps:
            for frame in step.expand_to_frames():
                frames[frame.frame] += frame

        return frames

    def increase_rerecord_count(self):
        persisted = self.read_rerecord_count()
        with open(self.rerecord_count_filename, 'w') as rerecord_count_file:
            rerecord_count_file.write(str(int(persisted) + 1))

    def read_rerecord_count(self):
        with open(self.rerecord_count_filename, 'r') as rerecord_count_file:
            return rerecord_count_file.read()

    def build_input_config(self):
        input_config = InputConfig()
        input_config.framerate_num = str(self.framerate)
        input_config.authors = ', '.join(self.authors)
        input_config.rerecord_count = self.read_rerecord_count()

        return input_config

    def record(self):
        self.increase_rerecord_count()

        return (lazy_mouse(self.build_frames()), self.build_input_config())
