

class FrameCounter:

    def __init__(self, start: int = 0):
        self.frame = start

    def advance(self, frames: int):
        self.frame += frames
        return self.frame

    def advance_to(self, frame: int):
        if frame > self.frame:
            self.frame = frame
            return self.frame
        else:
            return self.frame
