
class InputConfig:

    def __init__(self):
        self.authors = 'me'
        self.frame_count = '0'
        self.framerate_den = '1'
        self.framerate_num = '30'
        self.game_name = 'game'
        self.initial_time_nsec = '0'
        self.initial_time_sec = '1'
        self.keyboard_support = 'true'
        self.libtas_major_version = '1'
        self.libtas_minor_version = '3'
        self.libtas_patch_version = '4'
        self.mouse_support = 'false'
        self.nb_controllers = '0'
        self.rerecord_count = '0'
        self.savestate_frame_count = '0'

        self.mainthread_timetrack = Timetrack()
        self.secondarythread_timetrack = Timetrack()

    def fill_template(self) -> str:
        config = ''
        with open('ltm/inputConfig.ini', 'r') as config_file:
            config = config_file.read().format(config=self)
        return config


class Timetrack:

    def __init__(self):
        self.clock = '-1'
        self.clock_gettime = '-1'
        self.gettimeofday = '-1'
        self.sdl_getperformancecounter = '-1'
        self.sdl_getticks = '-1'
        self.time = '-1'
