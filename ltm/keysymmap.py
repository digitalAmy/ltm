# extensive list of XKeysym-Inputs compiled from https://www.cl.cam.ac.uk/~mgk25/ucs/keysymdef.h

# Copyright 1987, 1994, 1998  The Open Group

# Permission to use, copy, modify, distribute, and sell this software and its
# documentation for any purpose is hereby granted without fee, provided that
# the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# Except as contained in this notice, the name of The Open Group shall
# not be used in advertising or otherwise to promote the sale, use or
# other dealings in this Software without prior written authorization
# from The Open Group.


# Copyright 1987 by Digital Equipment Corporation, Maynard, Massachusetts

#                         All Rights Reserved

# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Digital not be
# used in advertising or publicity pertaining to distribution of the
# software without specific, written prior permission.

# DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
# DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
# SOFTWARE.

keysyms = {
    'BackSpace':                      'ff08',  # Back space, back char 
    'Tab':                            'ff09',
    'Linefeed':                       'ff0a',  # Linefeed, LF 
    'Clear':                          'ff0b',
    'Return':                         'ff0d',  # Return, enter 
    'Pause':                          'ff13',  # Pause, hold 
    'Scroll_Lock':                    'ff14',
    'Sys_Req':                        'ff15',
    'Escape':                         'ff1b',
    'Delete':                         'ffff',  # Delete, rubout 

    # International & multi-key character composition 

    'Multi_key': 'ff20',  # Multi-key character compose 
    'Codeinput': 'ff37',
    'SingleCandidate': 'ff3c',
    'MultipleCandidate': 'ff3d',
    'PreviousCandidate': 'ff3e',

    # Japanese keyboard support 

    'Kanji': 'ff21',  # Kanji, Kanji convert 
    'Muhenkan': 'ff22',  # Cancel Conversion 
    'Henkan_Mode': 'ff23',  # Start/Stop Conversion 
    'Henkan': 'ff23',  # Alias for Henkan_Mode 
    'Romaji': 'ff24',  # to Romaji 
    'Hiragana': 'ff25',  # to Hiragana 
    'Katakana': 'ff26',  # to Katakana 
    'Hiragana_Katakana': 'ff27',  # Hiragana/Katakana toggle 
    'Zenkaku': 'ff28',  # to Zenkaku 
    'Hankaku': 'ff29',  # to Hankaku 
    'Zenkaku_Hankaku': 'ff2a',  # Zenkaku/Hankaku toggle 
    'Touroku': 'ff2b',  # Add to Dictionary 
    'Massyo': 'ff2c',  # Delete from Dictionary 
    'Kana_Lock': 'ff2d',  # Kana Lock 
    'Kana_Shift': 'ff2e',  # Kana Shift 
    'Eisu_Shift': 'ff2f',  # Alphanumeric Shift 
    'Eisu_toggle': 'ff30',  # Alphanumeric toggle 
    'Kanji_Bangou': 'ff37',  # Codeinput 
    'Zen_Koho': 'ff3d',  # Multiple/All Candidate(s) 
    'Mae_Koho': 'ff3e',  # Previous Candidate 

    #': 'ff31 thru': 'ff3f are under KOREAN 

    # Cursor control & motion 

    'Home': 'ff50',
    'Left': 'ff51',  # Move left, left arrow 
    'Up': 'ff52',  # Move up, up arrow 
    'Right': 'ff53',  # Move right, right arrow 
    'Down': 'ff54',  # Move down, down arrow 
    'Prior': 'ff55',  # Prior, previous 
    'Page_Up': 'ff55',
    'Next': 'ff56',  # Next 
    'Page_Down': 'ff56',
    'End': 'ff57',  # EOL 
    'Begin': 'ff58',  # BOL 

    # Misc functions 

    'Select': 'ff60',  # Select, mark 
    'Print': 'ff61',
    'Execute': 'ff62',  # Execute, run, do 
    'Insert': 'ff63',  # Insert, insert here 
    'Undo': 'ff65',
    'Redo': 'ff66',  # Redo, again 
    'Menu': 'ff67',
    'Find': 'ff68',  # Find, search 
    'Cancel': 'ff69',  # Cancel, stop, abort, exit 
    'Help': 'ff6a',  # Help 
    'Break': 'ff6b',
    'Mode_switch': 'ff7e',  # Character set switch 
    'script_switch': 'ff7e',  # Alias for mode_switch 
    'Num_Lock': 'ff7f',

    # Keypad functions, keypad numbers cleverly chosen to map to ASCII 

    'KP_Space': 'ff80',  # Space 
    'KP_Tab': 'ff89',
    'KP_Enter': 'ff8d',  # Enter 
    'KP_F1': 'ff91',  # PF1, KP_A, ... 
    'KP_F2': 'ff92',
    'KP_F3': 'ff93',
    'KP_F4': 'ff94',
    'KP_Home': 'ff95',
    'KP_Left': 'ff96',
    'KP_Up': 'ff97',
    'KP_Right': 'ff98',
    'KP_Down': 'ff99',
    'KP_Prior': 'ff9a',
    'KP_Page_Up': 'ff9a',
    'KP_Next': 'ff9b',
    'KP_Page_Down': 'ff9b',
    'KP_End': 'ff9c',
    'KP_Begin': 'ff9d',
    'KP_Insert': 'ff9e',
    'KP_Delete': 'ff9f',
    'KP_Equal': 'ffbd',  # Equals 
    'KP_Multiply': 'ffaa',
    'KP_Add': 'ffab',
    'KP_Separator': 'ffac',  # Separator, often comma 
    'KP_Subtract': 'ffad',
    'KP_Decimal': 'ffae',
    'KP_Divide': 'ffaf',

    'KP_0': 'ffb0',
    'KP_1': 'ffb1',
    'KP_2': 'ffb2',
    'KP_3': 'ffb3',
    'KP_4': 'ffb4',
    'KP_5': 'ffb5',
    'KP_6': 'ffb6',
    'KP_7': 'ffb7',
    'KP_8': 'ffb8',
    'KP_9': 'ffb9',

    # Auxilliary functions; note the duplicate definitions for left and right
    # function keys;  Sun keyboards and a few other manufactures have such
    # function key groups on the left and/or right sides of the keyboard.
    # We've not found a keyboard with more than 35 function keys total.

    'F1': 'ffbe',
    'F2': 'ffbf',
    'F3': 'ffc0',
    'F4': 'ffc1',
    'F5': 'ffc2',
    'F6': 'ffc3',
    'F7': 'ffc4',
    'F8': 'ffc5',
    'F9': 'ffc6',
    'F10': 'ffc7',
    'F11': 'ffc8',
    'L1': 'ffc8',
    'F12': 'ffc9',
    'L2': 'ffc9',
    'F13': 'ffca',
    'L3': 'ffca',
    'F14': 'ffcb',
    'L4': 'ffcb',
    'F15': 'ffcc',
    'L5': 'ffcc',
    'F16': 'ffcd',
    'L6': 'ffcd',
    'F17': 'ffce',
    'L7': 'ffce',
    'F18': 'ffcf',
    'L8': 'ffcf',
    'F19': 'ffd0',
    'L9': 'ffd0',
    'F20': 'ffd1',
    'L10': 'ffd1',
    'F21': 'ffd2',
    'R1': 'ffd2',
    'F22': 'ffd3',
    'R2': 'ffd3',
    'F23': 'ffd4',
    'R3': 'ffd4',
    'F24': 'ffd5',
    'R4': 'ffd5',
    'F25': 'ffd6',
    'R5': 'ffd6',
    'F26': 'ffd7',
    'R6': 'ffd7',
    'F27': 'ffd8',
    'R7': 'ffd8',
    'F28': 'ffd9',
    'R8': 'ffd9',
    'F29': 'ffda',
    'R9': 'ffda',
    'F30': 'ffdb',
    'R10': 'ffdb',
    'F31': 'ffdc',
    'R11': 'ffdc',
    'F32': 'ffdd',
    'R12': 'ffdd',
    'F33': 'ffde',
    'R13': 'ffde',
    'F34': 'ffdf',
    'R14': 'ffdf',
    'F35': 'ffe0',
    'R15': 'ffe0',

    # Modifiers 

    'Shift_L': 'ffe1',  # Left shift 
    'Shift_R': 'ffe2',  # Right shift 
    'Control_L': 'ffe3',  # Left control 
    'Control_R': 'ffe4',  # Right control 
    'Caps_Lock': 'ffe5',  # Caps lock 
    'Shift_Lock': 'ffe6',  # Shift lock 

    'Meta_L': 'ffe7',  # Left meta 
    'Meta_R': 'ffe8',  # Right meta 
    'Alt_L': 'ffe9',  # Left alt 
    'Alt_R': 'ffea',  # Right alt 
    'Super_L': 'ffeb',  # Left super 
    'Super_R': 'ffec',  # Right super 
    'Hyper_L': 'ffed',  # Left hyper 
    'Hyper_R': 'ffee',  # Right hyper 
    
    # Keyboard (XKB) Extension function and modifier keys
    # (from Appendix C of "The X Keyboard Extension: Protocol Specification")
    # Byte 3 =': 'fe
 
    'ISO_Lock': 'fe01',
    'ISO_Level2_Latch': 'fe02',
    'ISO_Level3_Shift': 'fe03',
    'ISO_Level3_Latch': 'fe04',
    'ISO_Level3_Lock': 'fe05',
    'ISO_Group_Shift': 'ff7e',  # Alias for mode_switch 
    'ISO_Group_Latch': 'fe06',
    'ISO_Group_Lock': 'fe07',
    'ISO_Next_Group': 'fe08',
    'ISO_Next_Group_Lock': 'fe09',
    'ISO_Prev_Group': 'fe0a',
    'ISO_Prev_Group_Lock': 'fe0b',
    'ISO_First_Group': 'fe0c',
    'ISO_First_Group_Lock': 'fe0d',
    'ISO_Last_Group': 'fe0e',
    'ISO_Last_Group_Lock': 'fe0f',

    'ISO_Left_Tab': 'fe20',
    'ISO_Move_Line_Up': 'fe21',
    'ISO_Move_Line_Down': 'fe22',
    'ISO_Partial_Line_Up': 'fe23',
    'ISO_Partial_Line_Down': 'fe24',
    'ISO_Partial_Space_Left': 'fe25',
    'ISO_Partial_Space_Right': 'fe26',
    'ISO_Set_Margin_Left': 'fe27',
    'ISO_Set_Margin_Right': 'fe28',
    'ISO_Release_Margin_Left': 'fe29',
    'ISO_Release_Margin_Right': 'fe2a',
    'ISO_Release_Both_Margins': 'fe2b',
    'ISO_Fast_Cursor_Left': 'fe2c',
    'ISO_Fast_Cursor_Right': 'fe2d',
    'ISO_Fast_Cursor_Up': 'fe2e',
    'ISO_Fast_Cursor_Down': 'fe2f',
    'ISO_Continuous_Underline': 'fe30',
    'ISO_Discontinuous_Underline': 'fe31',
    'ISO_Emphasize': 'fe32',
    'ISO_Center_Object': 'fe33',
    'ISO_Enter': 'fe34',

    'dead_grave': 'fe50',
    'dead_acute': 'fe51',
    'dead_circumflex': 'fe52',
    'dead_tilde': 'fe53',
    'dead_macron': 'fe54',
    'dead_breve': 'fe55',
    'dead_abovedot': 'fe56',
    'dead_diaeresis': 'fe57',
    'dead_abovering': 'fe58',
    'dead_doubleacute': 'fe59',
    'dead_caron': 'fe5a',
    'dead_cedilla': 'fe5b',
    'dead_ogonek': 'fe5c',
    'dead_iota': 'fe5d',
    'dead_voiced_sound': 'fe5e',
    'dead_semivoiced_sound': 'fe5f',
    'dead_belowdot': 'fe60',
    'dead_hook': 'fe61',
    'dead_horn': 'fe62',

    'First_Virtual_Screen': 'fed0',
    'Prev_Virtual_Screen': 'fed1',
    'Next_Virtual_Screen': 'fed2',
    'Last_Virtual_Screen': 'fed4',
    'Terminate_Server': 'fed5',

    'AccessX_Enable': 'fe70',
    'AccessX_Feedback_Enable': 'fe71',
    'RepeatKeys_Enable': 'fe72',
    'SlowKeys_Enable': 'fe73',
    'BounceKeys_Enable': 'fe74',
    'StickyKeys_Enable': 'fe75',
    'MouseKeys_Enable': 'fe76',
    'MouseKeys_Accel_Enable': 'fe77',
    'Overlay1_Enable': 'fe78',
    'Overlay2_Enable': 'fe79',
    'AudibleBell_Enable': 'fe7a',

    'Pointer_Left': 'fee0',
    'Pointer_Right': 'fee1',
    'Pointer_Up': 'fee2',
    'Pointer_Down': 'fee3',
    'Pointer_UpLeft': 'fee4',
    'Pointer_UpRight': 'fee5',
    'Pointer_DownLeft': 'fee6',
    'Pointer_DownRight': 'fee7',
    'Pointer_Button_Dflt': 'fee8',
    'Pointer_Button1': 'fee9',
    'Pointer_Button2': 'feea',
    'Pointer_Button3': 'feeb',
    'Pointer_Button4': 'feec',
    'Pointer_Button5': 'feed',
    'Pointer_DblClick_Dflt': 'feee',
    'Pointer_DblClick1': 'feef',
    'Pointer_DblClick2': 'fef0',
    'Pointer_DblClick3': 'fef1',
    'Pointer_DblClick4': 'fef2',
    'Pointer_DblClick5': 'fef3',
    'Pointer_Drag_Dflt': 'fef4',
    'Pointer_Drag1': 'fef5',
    'Pointer_Drag2': 'fef6',
    'Pointer_Drag3': 'fef7',
    'Pointer_Drag4': 'fef8',
    'Pointer_Drag5': 'fefd',

    'Pointer_EnableKeys': 'fef9',
    'Pointer_Accelerate': 'fefa',
    'Pointer_DfltBtnNext': 'fefb',
    'Pointer_DfltBtnPrev': 'fefc',

    # 3270 Terminal Keys
    # Byte 3 =': 'fd
 
    '3270_Duplicate': 'fd01',
    '3270_FieldMark': 'fd02',
    '3270_Right2': 'fd03',
    '3270_Left2': 'fd04',
    '3270_BackTab': 'fd05',
    '3270_EraseEOF': 'fd06',
    '3270_EraseInput': 'fd07',
    '3270_Reset': 'fd08',
    '3270_Quit': 'fd09',
    '3270_PA1': 'fd0a',
    '3270_PA2': 'fd0b',
    '3270_PA3': 'fd0c',
    '3270_Test': 'fd0d',
    '3270_Attn': 'fd0e',
    '3270_CursorBlink': 'fd0f',
    '3270_AltCursor': 'fd10',
    '3270_KeyClick': 'fd11',
    '3270_Jump': 'fd12',
    '3270_Ident': 'fd13',
    '3270_Rule': 'fd14',
    '3270_Copy': 'fd15',
    '3270_Play': 'fd16',
    '3270_Setup': 'fd17',
    '3270_Record': 'fd18',
    '3270_ChangeScreen': 'fd19',
    '3270_DeleteWord': 'fd1a',
    '3270_ExSelect': 'fd1b',
    '3270_CursorSelect': 'fd1c',
    '3270_PrintScreen': 'fd1d',
    '3270_Enter': 'fd1e',
 
    # Latin 1
    # (ISO/IEC 8859-1 = Unicode U+0020..U+00FF)
    # Byte 3 = 0

    'space': '0020',  # U+0020 SPACE 
    'exclam': '0021',  # U+0021 EXCLAMATION MARK 
    'quotedbl': '0022',  # U+0022 QUOTATION MARK 
    'numbersign': '0023',  # U+0023 NUMBER SIGN 
    'dollar': '0024',  # U+0024 DOLLAR SIGN 
    'percent': '0025',  # U+0025 PERCENT SIGN 
    'ampersand': '0026',  # U+0026 AMPERSAND 
    'apostrophe': '0027',  # U+0027 APOSTROPHE 
    'quoteright': '0027',  # deprecated 
    'parenleft': '0028',  # U+0028 LEFT PARENTHESIS 
    'parenright': '0029',  # U+0029 RIGHT PARENTHESIS 
    'asterisk': '002a',  # U+002A ASTERISK 
    'plus': '002b',  # U+002B PLUS SIGN 
    'comma': '002c',  # U+002C COMMA 
    'minus': '002d',  # U+002D HYPHEN-MINUS 
    'period': '002e',  # U+002E FULL STOP 
    'slash': '002f',  # U+002F SOLIDUS 
    '0': '0030',  # U+0030 DIGIT ZERO 
    '1': '0031',  # U+0031 DIGIT ONE 
    '2': '0032',  # U+0032 DIGIT TWO 
    '3': '0033',  # U+0033 DIGIT THREE 
    '4': '0034',  # U+0034 DIGIT FOUR 
    '5': '0035',  # U+0035 DIGIT FIVE 
    '6': '0036',  # U+0036 DIGIT SIX 
    '7': '0037',  # U+0037 DIGIT SEVEN 
    '8': '0038',  # U+0038 DIGIT EIGHT 
    '9': '0039',  # U+0039 DIGIT NINE 
    'colon': '003a',  # U+003A COLON 
    'semicolon': '003b',  # U+003B SEMICOLON 
    'less': '003c',  # U+003C LESS-THAN SIGN 
    'equal': '003d',  # U+003D EQUALS SIGN 
    'greater': '003e',  # U+003E GREATER-THAN SIGN 
    'question': '003f',  # U+003F QUESTION MARK 
    'at': '0040',  # U+0040 COMMERCIAL AT 
    'A': '0041',  # U+0041 LATIN CAPITAL LETTER A 
    'B': '0042',  # U+0042 LATIN CAPITAL LETTER B 
    'C': '0043',  # U+0043 LATIN CAPITAL LETTER C 
    'D': '0044',  # U+0044 LATIN CAPITAL LETTER D 
    'E': '0045',  # U+0045 LATIN CAPITAL LETTER E 
    'F': '0046',  # U+0046 LATIN CAPITAL LETTER F 
    'G': '0047',  # U+0047 LATIN CAPITAL LETTER G 
    'H': '0048',  # U+0048 LATIN CAPITAL LETTER H 
    'I': '0049',  # U+0049 LATIN CAPITAL LETTER I 
    'J': '004a',  # U+004A LATIN CAPITAL LETTER J 
    'K': '004b',  # U+004B LATIN CAPITAL LETTER K 
    'L': '004c',  # U+004C LATIN CAPITAL LETTER L 
    'M': '004d',  # U+004D LATIN CAPITAL LETTER M 
    'N': '004e',  # U+004E LATIN CAPITAL LETTER N 
    'O': '004f',  # U+004F LATIN CAPITAL LETTER O 
    'P': '0050',  # U+0050 LATIN CAPITAL LETTER P 
    'Q': '0051',  # U+0051 LATIN CAPITAL LETTER Q 
    'R': '0052',  # U+0052 LATIN CAPITAL LETTER R 
    'S': '0053',  # U+0053 LATIN CAPITAL LETTER S 
    'T': '0054',  # U+0054 LATIN CAPITAL LETTER T 
    'U': '0055',  # U+0055 LATIN CAPITAL LETTER U 
    'V': '0056',  # U+0056 LATIN CAPITAL LETTER V 
    'W': '0057',  # U+0057 LATIN CAPITAL LETTER W 
    'X': '0058',  # U+0058 LATIN CAPITAL LETTER X 
    'Y': '0059',  # U+0059 LATIN CAPITAL LETTER Y 
    'Z': '005a',  # U+005A LATIN CAPITAL LETTER Z 
    'bracketleft': '005b',  # U+005B LEFT SQUARE BRACKET 
    'backslash': '005c',  # U+005C REVERSE SOLIDUS 
    'bracketright': '005d',  # U+005D RIGHT SQUARE BRACKET 
    'asciicircum': '005e',  # U+005E CIRCUMFLEX ACCENT 
    'underscore': '005f',  # U+005F LOW LINE 
    'grave': '0060',  # U+0060 GRAVE ACCENT 
    'quoteleft': '0060',  # deprecated 
    'a': '0061',  # U+0061 LATIN SMALL LETTER A 
    'b': '0062',  # U+0062 LATIN SMALL LETTER B 
    'c': '0063',  # U+0063 LATIN SMALL LETTER C 
    'd': '0064',  # U+0064 LATIN SMALL LETTER D 
    'e': '0065',  # U+0065 LATIN SMALL LETTER E 
    'f': '0066',  # U+0066 LATIN SMALL LETTER F 
    'g': '0067',  # U+0067 LATIN SMALL LETTER G 
    'h': '0068',  # U+0068 LATIN SMALL LETTER H 
    'i': '0069',  # U+0069 LATIN SMALL LETTER I 
    'j': '006a',  # U+006A LATIN SMALL LETTER J 
    'k': '006b',  # U+006B LATIN SMALL LETTER K 
    'l': '006c',  # U+006C LATIN SMALL LETTER L 
    'm': '006d',  # U+006D LATIN SMALL LETTER M 
    'n': '006e',  # U+006E LATIN SMALL LETTER N 
    'o': '006f',  # U+006F LATIN SMALL LETTER O 
    'p': '0070',  # U+0070 LATIN SMALL LETTER P 
    'q': '0071',  # U+0071 LATIN SMALL LETTER Q 
    'r': '0072',  # U+0072 LATIN SMALL LETTER R 
    's': '0073',  # U+0073 LATIN SMALL LETTER S 
    't': '0074',  # U+0074 LATIN SMALL LETTER T 
    'u': '0075',  # U+0075 LATIN SMALL LETTER U 
    'v': '0076',  # U+0076 LATIN SMALL LETTER V 
    'w': '0077',  # U+0077 LATIN SMALL LETTER W 
    'x': '0078',  # U+0078 LATIN SMALL LETTER X 
    'y': '0079',  # U+0079 LATIN SMALL LETTER Y 
    'z': '007a',  # U+007A LATIN SMALL LETTER Z 
    'braceleft': '007b',  # U+007B LEFT CURLY BRACKET 
    'bar': '007c',  # U+007C VERTICAL LINE 
    'braceright': '007d',  # U+007D RIGHT CURLY BRACKET 
    'asciitilde': '007e',  # U+007E TILDE 

    'nobreakspace': '00a0',  # U+00A0 NO-BREAK SPACE 
    'exclamdown': '00a1',  # U+00A1 INVERTED EXCLAMATION MARK 
    'cent': '00a2',  # U+00A2 CENT SIGN 
    'sterling': '00a3',  # U+00A3 POUND SIGN 
    'currency': '00a4',  # U+00A4 CURRENCY SIGN 
    'yen': '00a5',  # U+00A5 YEN SIGN 
    'brokenbar': '00a6',  # U+00A6 BROKEN BAR 
    'section': '00a7',  # U+00A7 SECTION SIGN 
    'diaeresis': '00a8',  # U+00A8 DIAERESIS 
    'copyright': '00a9',  # U+00A9 COPYRIGHT SIGN 
    'ordfeminine': '00aa',  # U+00AA FEMININE ORDINAL INDICATOR 
    'guillemotleft': '00ab',  # U+00AB LEFT-POINTING DOUBLE ANGLE QUOTATION MARK 
    'notsign': '00ac',  # U+00AC NOT SIGN 
    'hyphen': '00ad',  # U+00AD SOFT HYPHEN 
    'registered': '00ae',  # U+00AE REGISTERED SIGN 
    'macron': '00af',  # U+00AF MACRON 
    'degree': '00b0',  # U+00B0 DEGREE SIGN 
    'plusminus': '00b1',  # U+00B1 PLUS-MINUS SIGN 
    'twosuperior': '00b2',  # U+00B2 SUPERSCRIPT TWO 
    'threesuperior': '00b3',  # U+00B3 SUPERSCRIPT THREE 
    'acute': '00b4',  # U+00B4 ACUTE ACCENT 
    'mu': '00b5',  # U+00B5 MICRO SIGN 
    'paragraph': '00b6',  # U+00B6 PILCROW SIGN 
    'periodcentered': '00b7',  # U+00B7 MIDDLE DOT 
    'cedilla': '00b8',  # U+00B8 CEDILLA 
    'onesuperior': '00b9',  # U+00B9 SUPERSCRIPT ONE 
    'masculine': '00ba',  # U+00BA MASCULINE ORDINAL INDICATOR 
    'guillemotright': '00bb',  # U+00BB RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK 
    'onequarter': '00bc',  # U+00BC VULGAR FRACTION ONE QUARTER 
    'onehalf': '00bd',  # U+00BD VULGAR FRACTION ONE HALF 
    'threequarters': '00be',  # U+00BE VULGAR FRACTION THREE QUARTERS 
    'questiondown': '00bf',  # U+00BF INVERTED QUESTION MARK 
    'Agrave': '00c0',  # U+00C0 LATIN CAPITAL LETTER A WITH GRAVE 
    'Aacute': '00c1',  # U+00C1 LATIN CAPITAL LETTER A WITH ACUTE 
    'Acircumflex': '00c2',  # U+00C2 LATIN CAPITAL LETTER A WITH CIRCUMFLEX 
    'Atilde': '00c3',  # U+00C3 LATIN CAPITAL LETTER A WITH TILDE 
    'Adiaeresis': '00c4',  # U+00C4 LATIN CAPITAL LETTER A WITH DIAERESIS 
    'Aring': '00c5',  # U+00C5 LATIN CAPITAL LETTER A WITH RING ABOVE 
    'AE': '00c6',  # U+00C6 LATIN CAPITAL LETTER AE 
    'Ccedilla': '00c7',  # U+00C7 LATIN CAPITAL LETTER C WITH CEDILLA 
    'Egrave': '00c8',  # U+00C8 LATIN CAPITAL LETTER E WITH GRAVE 
    'Eacute': '00c9',  # U+00C9 LATIN CAPITAL LETTER E WITH ACUTE 
    'Ecircumflex': '00ca',  # U+00CA LATIN CAPITAL LETTER E WITH CIRCUMFLEX 
    'Ediaeresis': '00cb',  # U+00CB LATIN CAPITAL LETTER E WITH DIAERESIS 
    'Igrave': '00cc',  # U+00CC LATIN CAPITAL LETTER I WITH GRAVE 
    'Iacute': '00cd',  # U+00CD LATIN CAPITAL LETTER I WITH ACUTE 
    'Icircumflex': '00ce',  # U+00CE LATIN CAPITAL LETTER I WITH CIRCUMFLEX 
    'Idiaeresis': '00cf',  # U+00CF LATIN CAPITAL LETTER I WITH DIAERESIS 
    'ETH': '00d0',  # U+00D0 LATIN CAPITAL LETTER ETH 
    'Eth': '00d0',  # deprecated 
    'Ntilde': '00d1',  # U+00D1 LATIN CAPITAL LETTER N WITH TILDE 
    'Ograve': '00d2',  # U+00D2 LATIN CAPITAL LETTER O WITH GRAVE 
    'Oacute': '00d3',  # U+00D3 LATIN CAPITAL LETTER O WITH ACUTE 
    'Ocircumflex': '00d4',  # U+00D4 LATIN CAPITAL LETTER O WITH CIRCUMFLEX 
    'Otilde': '00d5',  # U+00D5 LATIN CAPITAL LETTER O WITH TILDE 
    'Odiaeresis': '00d6',  # U+00D6 LATIN CAPITAL LETTER O WITH DIAERESIS 
    'multiply': '00d7',  # U+00D7 MULTIPLICATION SIGN 
    'Oslash': '00d8',  # U+00D8 LATIN CAPITAL LETTER O WITH STROKE 
    'Ooblique': '00d8',  # U+00D8 LATIN CAPITAL LETTER O WITH STROKE 
    'Ugrave': '00d9',  # U+00D9 LATIN CAPITAL LETTER U WITH GRAVE 
    'Uacute': '00da',  # U+00DA LATIN CAPITAL LETTER U WITH ACUTE 
    'Ucircumflex': '00db',  # U+00DB LATIN CAPITAL LETTER U WITH CIRCUMFLEX 
    'Udiaeresis': '00dc',  # U+00DC LATIN CAPITAL LETTER U WITH DIAERESIS 
    'Yacute': '00dd',  # U+00DD LATIN CAPITAL LETTER Y WITH ACUTE 
    'THORN': '00de',  # U+00DE LATIN CAPITAL LETTER THORN 
    'Thorn': '00de',  # deprecated 
    'ssharp': '00df',  # U+00DF LATIN SMALL LETTER SHARP S 
    'agrave': '00e0',  # U+00E0 LATIN SMALL LETTER A WITH GRAVE 
    'aacute': '00e1',  # U+00E1 LATIN SMALL LETTER A WITH ACUTE 
    'acircumflex': '00e2',  # U+00E2 LATIN SMALL LETTER A WITH CIRCUMFLEX 
    'atilde': '00e3',  # U+00E3 LATIN SMALL LETTER A WITH TILDE 
    'adiaeresis': '00e4',  # U+00E4 LATIN SMALL LETTER A WITH DIAERESIS 
    'aring': '00e5',  # U+00E5 LATIN SMALL LETTER A WITH RING ABOVE 
    'ae': '00e6',  # U+00E6 LATIN SMALL LETTER AE 
    'ccedilla': '00e7',  # U+00E7 LATIN SMALL LETTER C WITH CEDILLA 
    'egrave': '00e8',  # U+00E8 LATIN SMALL LETTER E WITH GRAVE 
    'eacute': '00e9',  # U+00E9 LATIN SMALL LETTER E WITH ACUTE 
    'ecircumflex': '00ea',  # U+00EA LATIN SMALL LETTER E WITH CIRCUMFLEX 
    'ediaeresis': '00eb',  # U+00EB LATIN SMALL LETTER E WITH DIAERESIS 
    'igrave': '00ec',  # U+00EC LATIN SMALL LETTER I WITH GRAVE 
    'iacute': '00ed',  # U+00ED LATIN SMALL LETTER I WITH ACUTE 
    'icircumflex': '00ee',  # U+00EE LATIN SMALL LETTER I WITH CIRCUMFLEX 
    'idiaeresis': '00ef',  # U+00EF LATIN SMALL LETTER I WITH DIAERESIS 
    'eth': '00f0',  # U+00F0 LATIN SMALL LETTER ETH 
    'ntilde': '00f1',  # U+00F1 LATIN SMALL LETTER N WITH TILDE 
    'ograve': '00f2',  # U+00F2 LATIN SMALL LETTER O WITH GRAVE 
    'oacute': '00f3',  # U+00F3 LATIN SMALL LETTER O WITH ACUTE 
    'ocircumflex': '00f4',  # U+00F4 LATIN SMALL LETTER O WITH CIRCUMFLEX 
    'otilde': '00f5',  # U+00F5 LATIN SMALL LETTER O WITH TILDE 
    'odiaeresis': '00f6',  # U+00F6 LATIN SMALL LETTER O WITH DIAERESIS 
    'division': '00f7',  # U+00F7 DIVISION SIGN 
    'oslash': '00f8',  # U+00F8 LATIN SMALL LETTER O WITH STROKE 
    'ooblique': '00f8',  # U+00F8 LATIN SMALL LETTER O WITH STROKE 
    'ugrave': '00f9',  # U+00F9 LATIN SMALL LETTER U WITH GRAVE 
    'uacute': '00fa',  # U+00FA LATIN SMALL LETTER U WITH ACUTE 
    'ucircumflex': '00fb',  # U+00FB LATIN SMALL LETTER U WITH CIRCUMFLEX 
    'udiaeresis': '00fc',  # U+00FC LATIN SMALL LETTER U WITH DIAERESIS 
    'yacute': '00fd',  # U+00FD LATIN SMALL LETTER Y WITH ACUTE 
    'thorn': '00fe',  # U+00FE LATIN SMALL LETTER THORN 
    'ydiaeresis': '00ff',  # U+00FF LATIN SMALL LETTER Y WITH DIAERESIS 
 
    # Latin 2
    # Byte 3 = 1
 
    'Aogonek': '01a1',  # U+0104 LATIN CAPITAL LETTER A WITH OGONEK 
    'breve': '01a2',  # U+02D8 BREVE 
    'Lstroke': '01a3',  # U+0141 LATIN CAPITAL LETTER L WITH STROKE 
    'Lcaron': '01a5',  # U+013D LATIN CAPITAL LETTER L WITH CARON 
    'Sacute': '01a6',  # U+015A LATIN CAPITAL LETTER S WITH ACUTE 
    'Scaron': '01a9',  # U+0160 LATIN CAPITAL LETTER S WITH CARON 
    'Scedilla': '01aa',  # U+015E LATIN CAPITAL LETTER S WITH CEDILLA 
    'Tcaron': '01ab',  # U+0164 LATIN CAPITAL LETTER T WITH CARON 
    'Zacute': '01ac',  # U+0179 LATIN CAPITAL LETTER Z WITH ACUTE 
    'Zcaron': '01ae',  # U+017D LATIN CAPITAL LETTER Z WITH CARON 
    'Zabovedot': '01af',  # U+017B LATIN CAPITAL LETTER Z WITH DOT ABOVE 
    'aogonek': '01b1',  # U+0105 LATIN SMALL LETTER A WITH OGONEK 
    'ogonek': '01b2',  # U+02DB OGONEK 
    'lstroke': '01b3',  # U+0142 LATIN SMALL LETTER L WITH STROKE 
    'lcaron': '01b5',  # U+013E LATIN SMALL LETTER L WITH CARON 
    'sacute': '01b6',  # U+015B LATIN SMALL LETTER S WITH ACUTE 
    'caron': '01b7',  # U+02C7 CARON 
    'scaron': '01b9',  # U+0161 LATIN SMALL LETTER S WITH CARON 
    'scedilla': '01ba',  # U+015F LATIN SMALL LETTER S WITH CEDILLA 
    'tcaron': '01bb',  # U+0165 LATIN SMALL LETTER T WITH CARON 
    'zacute': '01bc',  # U+017A LATIN SMALL LETTER Z WITH ACUTE 
    'doubleacute': '01bd',  # U+02DD DOUBLE ACUTE ACCENT 
    'zcaron': '01be',  # U+017E LATIN SMALL LETTER Z WITH CARON 
    'zabovedot': '01bf',  # U+017C LATIN SMALL LETTER Z WITH DOT ABOVE 
    'Racute': '01c0',  # U+0154 LATIN CAPITAL LETTER R WITH ACUTE 
    'Abreve': '01c3',  # U+0102 LATIN CAPITAL LETTER A WITH BREVE 
    'Lacute': '01c5',  # U+0139 LATIN CAPITAL LETTER L WITH ACUTE 
    'Cacute': '01c6',  # U+0106 LATIN CAPITAL LETTER C WITH ACUTE 
    'Ccaron': '01c8',  # U+010C LATIN CAPITAL LETTER C WITH CARON 
    'Eogonek': '01ca',  # U+0118 LATIN CAPITAL LETTER E WITH OGONEK 
    'Ecaron': '01cc',  # U+011A LATIN CAPITAL LETTER E WITH CARON 
    'Dcaron': '01cf',  # U+010E LATIN CAPITAL LETTER D WITH CARON 
    'Dstroke': '01d0',  # U+0110 LATIN CAPITAL LETTER D WITH STROKE 
    'Nacute': '01d1',  # U+0143 LATIN CAPITAL LETTER N WITH ACUTE 
    'Ncaron': '01d2',  # U+0147 LATIN CAPITAL LETTER N WITH CARON 
    'Odoubleacute': '01d5',  # U+0150 LATIN CAPITAL LETTER O WITH DOUBLE ACUTE 
    'Rcaron': '01d8',  # U+0158 LATIN CAPITAL LETTER R WITH CARON 
    'Uring': '01d9',  # U+016E LATIN CAPITAL LETTER U WITH RING ABOVE 
    'Udoubleacute': '01db',  # U+0170 LATIN CAPITAL LETTER U WITH DOUBLE ACUTE 
    'Tcedilla': '01de',  # U+0162 LATIN CAPITAL LETTER T WITH CEDILLA 
    'racute': '01e0',  # U+0155 LATIN SMALL LETTER R WITH ACUTE 
    'abreve': '01e3',  # U+0103 LATIN SMALL LETTER A WITH BREVE 
    'lacute': '01e5',  # U+013A LATIN SMALL LETTER L WITH ACUTE 
    'cacute': '01e6',  # U+0107 LATIN SMALL LETTER C WITH ACUTE 
    'ccaron': '01e8',  # U+010D LATIN SMALL LETTER C WITH CARON 
    'eogonek': '01ea',  # U+0119 LATIN SMALL LETTER E WITH OGONEK 
    'ecaron': '01ec',  # U+011B LATIN SMALL LETTER E WITH CARON 
    'dcaron': '01ef',  # U+010F LATIN SMALL LETTER D WITH CARON 
    'dstroke': '01f0',  # U+0111 LATIN SMALL LETTER D WITH STROKE 
    'nacute': '01f1',  # U+0144 LATIN SMALL LETTER N WITH ACUTE 
    'ncaron': '01f2',  # U+0148 LATIN SMALL LETTER N WITH CARON 
    'odoubleacute': '01f5',  # U+0151 LATIN SMALL LETTER O WITH DOUBLE ACUTE 
    'udoubleacute': '01fb',  # U+0171 LATIN SMALL LETTER U WITH DOUBLE ACUTE 
    'rcaron': '01f8',  # U+0159 LATIN SMALL LETTER R WITH CARON 
    'uring': '01f9',  # U+016F LATIN SMALL LETTER U WITH RING ABOVE 
    'tcedilla': '01fe',  # U+0163 LATIN SMALL LETTER T WITH CEDILLA 
    'abovedot': '01ff',  # U+02D9 DOT ABOVE 

    # Latin 3
    # Byte 3 = 2
 
    'Hstroke': '02a1',  # U+0126 LATIN CAPITAL LETTER H WITH STROKE 
    'Hcircumflex': '02a6',  # U+0124 LATIN CAPITAL LETTER H WITH CIRCUMFLEX 
    'Iabovedot': '02a9',  # U+0130 LATIN CAPITAL LETTER I WITH DOT ABOVE 
    'Gbreve': '02ab',  # U+011E LATIN CAPITAL LETTER G WITH BREVE 
    'Jcircumflex': '02ac',  # U+0134 LATIN CAPITAL LETTER J WITH CIRCUMFLEX 
    'hstroke': '02b1',  # U+0127 LATIN SMALL LETTER H WITH STROKE 
    'hcircumflex': '02b6',  # U+0125 LATIN SMALL LETTER H WITH CIRCUMFLEX 
    'idotless': '02b9',  # U+0131 LATIN SMALL LETTER DOTLESS I 
    'gbreve': '02bb',  # U+011F LATIN SMALL LETTER G WITH BREVE 
    'jcircumflex': '02bc',  # U+0135 LATIN SMALL LETTER J WITH CIRCUMFLEX 
    'Cabovedot': '02c5',  # U+010A LATIN CAPITAL LETTER C WITH DOT ABOVE 
    'Ccircumflex': '02c6',  # U+0108 LATIN CAPITAL LETTER C WITH CIRCUMFLEX 
    'Gabovedot': '02d5',  # U+0120 LATIN CAPITAL LETTER G WITH DOT ABOVE 
    'Gcircumflex': '02d8',  # U+011C LATIN CAPITAL LETTER G WITH CIRCUMFLEX 
    'Ubreve': '02dd',  # U+016C LATIN CAPITAL LETTER U WITH BREVE 
    'Scircumflex': '02de',  # U+015C LATIN CAPITAL LETTER S WITH CIRCUMFLEX 
    'cabovedot': '02e5',  # U+010B LATIN SMALL LETTER C WITH DOT ABOVE 
    'ccircumflex': '02e6',  # U+0109 LATIN SMALL LETTER C WITH CIRCUMFLEX 
    'gabovedot': '02f5',  # U+0121 LATIN SMALL LETTER G WITH DOT ABOVE 
    'gcircumflex': '02f8',  # U+011D LATIN SMALL LETTER G WITH CIRCUMFLEX 
    'ubreve': '02fd',  # U+016D LATIN SMALL LETTER U WITH BREVE 
    'scircumflex': '02fe',  # U+015D LATIN SMALL LETTER S WITH CIRCUMFLEX 

    # Latin 4
    # Byte 3 = 3

    'kra': '03a2',  # U+0138 LATIN SMALL LETTER KRA 
    'kappa': '03a2',  # deprecated 
    'Rcedilla': '03a3',  # U+0156 LATIN CAPITAL LETTER R WITH CEDILLA 
    'Itilde': '03a5',  # U+0128 LATIN CAPITAL LETTER I WITH TILDE 
    'Lcedilla': '03a6',  # U+013B LATIN CAPITAL LETTER L WITH CEDILLA 
    'Emacron': '03aa',  # U+0112 LATIN CAPITAL LETTER E WITH MACRON 
    'Gcedilla': '03ab',  # U+0122 LATIN CAPITAL LETTER G WITH CEDILLA 
    'Tslash': '03ac',  # U+0166 LATIN CAPITAL LETTER T WITH STROKE 
    'rcedilla': '03b3',  # U+0157 LATIN SMALL LETTER R WITH CEDILLA 
    'itilde': '03b5',  # U+0129 LATIN SMALL LETTER I WITH TILDE 
    'lcedilla': '03b6',  # U+013C LATIN SMALL LETTER L WITH CEDILLA 
    'emacron': '03ba',  # U+0113 LATIN SMALL LETTER E WITH MACRON 
    'gcedilla': '03bb',  # U+0123 LATIN SMALL LETTER G WITH CEDILLA 
    'tslash': '03bc',  # U+0167 LATIN SMALL LETTER T WITH STROKE 
    'ENG': '03bd',  # U+014A LATIN CAPITAL LETTER ENG 
    'eng': '03bf',  # U+014B LATIN SMALL LETTER ENG 
    'Amacron': '03c0',  # U+0100 LATIN CAPITAL LETTER A WITH MACRON 
    'Iogonek': '03c7',  # U+012E LATIN CAPITAL LETTER I WITH OGONEK 
    'Eabovedot': '03cc',  # U+0116 LATIN CAPITAL LETTER E WITH DOT ABOVE 
    'Imacron': '03cf',  # U+012A LATIN CAPITAL LETTER I WITH MACRON 
    'Ncedilla': '03d1',  # U+0145 LATIN CAPITAL LETTER N WITH CEDILLA 
    'Omacron': '03d2',  # U+014C LATIN CAPITAL LETTER O WITH MACRON 
    'Kcedilla': '03d3',  # U+0136 LATIN CAPITAL LETTER K WITH CEDILLA 
    'Uogonek': '03d9',  # U+0172 LATIN CAPITAL LETTER U WITH OGONEK 
    'Utilde': '03dd',  # U+0168 LATIN CAPITAL LETTER U WITH TILDE 
    'Umacron': '03de',  # U+016A LATIN CAPITAL LETTER U WITH MACRON 
    'amacron': '03e0',  # U+0101 LATIN SMALL LETTER A WITH MACRON 
    'iogonek': '03e7',  # U+012F LATIN SMALL LETTER I WITH OGONEK 
    'eabovedot': '03ec',  # U+0117 LATIN SMALL LETTER E WITH DOT ABOVE 
    'imacron': '03ef',  # U+012B LATIN SMALL LETTER I WITH MACRON 
    'ncedilla': '03f1',  # U+0146 LATIN SMALL LETTER N WITH CEDILLA 
    'omacron': '03f2',  # U+014D LATIN SMALL LETTER O WITH MACRON 
    'kcedilla': '03f3',  # U+0137 LATIN SMALL LETTER K WITH CEDILLA 
    'uogonek': '03f9',  # U+0173 LATIN SMALL LETTER U WITH OGONEK 
    'utilde': '03fd',  # U+0169 LATIN SMALL LETTER U WITH TILDE 
    'umacron': '03fe',  # U+016B LATIN SMALL LETTER U WITH MACRON 

    # Latin 8
    
    'Babovedot': '1001e02',  # U+1E02 LATIN CAPITAL LETTER B WITH DOT ABOVE 
    'babovedot': '1001e03',  # U+1E03 LATIN SMALL LETTER B WITH DOT ABOVE 
    'Dabovedot': '1001e0a',  # U+1E0A LATIN CAPITAL LETTER D WITH DOT ABOVE 
    'Wgrave': '1001e80',  # U+1E80 LATIN CAPITAL LETTER W WITH GRAVE 
    'Wacute': '1001e82',  # U+1E82 LATIN CAPITAL LETTER W WITH ACUTE 
    'dabovedot': '1001e0b',  # U+1E0B LATIN SMALL LETTER D WITH DOT ABOVE 
    'Ygrave': '1001ef2',  # U+1EF2 LATIN CAPITAL LETTER Y WITH GRAVE 
    'Fabovedot': '1001e1e',  # U+1E1E LATIN CAPITAL LETTER F WITH DOT ABOVE 
    'fabovedot': '1001e1f',  # U+1E1F LATIN SMALL LETTER F WITH DOT ABOVE 
    'Mabovedot': '1001e40',  # U+1E40 LATIN CAPITAL LETTER M WITH DOT ABOVE 
    'mabovedot': '1001e41',  # U+1E41 LATIN SMALL LETTER M WITH DOT ABOVE 
    'Pabovedot': '1001e56',  # U+1E56 LATIN CAPITAL LETTER P WITH DOT ABOVE 
    'wgrave': '1001e81',  # U+1E81 LATIN SMALL LETTER W WITH GRAVE 
    'pabovedot': '1001e57',  # U+1E57 LATIN SMALL LETTER P WITH DOT ABOVE 
    'wacute': '1001e83',  # U+1E83 LATIN SMALL LETTER W WITH ACUTE 
    'Sabovedot': '1001e60',  # U+1E60 LATIN CAPITAL LETTER S WITH DOT ABOVE 
    'ygrave': '1001ef3',  # U+1EF3 LATIN SMALL LETTER Y WITH GRAVE 
    'Wdiaeresis': '1001e84',  # U+1E84 LATIN CAPITAL LETTER W WITH DIAERESIS 
    'wdiaeresis': '1001e85',  # U+1E85 LATIN SMALL LETTER W WITH DIAERESIS 
    'sabovedot': '1001e61',  # U+1E61 LATIN SMALL LETTER S WITH DOT ABOVE 
    'Wcircumflex': '1000174',  # U+0174 LATIN CAPITAL LETTER W WITH CIRCUMFLEX 
    'Tabovedot': '1001e6a',  # U+1E6A LATIN CAPITAL LETTER T WITH DOT ABOVE 
    'Ycircumflex': '1000176',  # U+0176 LATIN CAPITAL LETTER Y WITH CIRCUMFLEX 
    'wcircumflex': '1000175',  # U+0175 LATIN SMALL LETTER W WITH CIRCUMFLEX 
    'tabovedot': '1001e6b',  # U+1E6B LATIN SMALL LETTER T WITH DOT ABOVE 
    'ycircumflex': '1000177',  # U+0177 LATIN SMALL LETTER Y WITH CIRCUMFLEX 

    # Latin 9
    # Byte 3 =': '13
    
    'OE': '13bc',  # U+0152 LATIN CAPITAL LIGATURE OE 
    'oe': '13bd',  # U+0153 LATIN SMALL LIGATURE OE 
    'Ydiaeresis': '13be',  # U+0178 LATIN CAPITAL LETTER Y WITH DIAERESIS 

    # Katakana
    # Byte 3 = 4
    
    'overline': '047e',  # U+203E OVERLINE 
    'kana_fullstop': '04a1',  # U+3002 IDEOGRAPHIC FULL STOP 
    'kana_openingbracket': '04a2',  # U+300C LEFT CORNER BRACKET 
    'kana_closingbracket': '04a3',  # U+300D RIGHT CORNER BRACKET 
    'kana_comma': '04a4',  # U+3001 IDEOGRAPHIC COMMA 
    'kana_conjunctive': '04a5',  # U+30FB KATAKANA MIDDLE DOT 
    'kana_middledot': '04a5',  # deprecated 
    'kana_WO': '04a6',  # U+30F2 KATAKANA LETTER WO 
    'kana_a': '04a7',  # U+30A1 KATAKANA LETTER SMALL A 
    'kana_i': '04a8',  # U+30A3 KATAKANA LETTER SMALL I 
    'kana_u': '04a9',  # U+30A5 KATAKANA LETTER SMALL U 
    'kana_e': '04aa',  # U+30A7 KATAKANA LETTER SMALL E 
    'kana_o': '04ab',  # U+30A9 KATAKANA LETTER SMALL O 
    'kana_ya': '04ac',  # U+30E3 KATAKANA LETTER SMALL YA 
    'kana_yu': '04ad',  # U+30E5 KATAKANA LETTER SMALL YU 
    'kana_yo': '04ae',  # U+30E7 KATAKANA LETTER SMALL YO 
    'kana_tsu': '04af',  # U+30C3 KATAKANA LETTER SMALL TU 
    'kana_tu': '04af',  # deprecated 
    'prolongedsound': '04b0',  # U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK 
    'kana_A': '04b1',  # U+30A2 KATAKANA LETTER A 
    'kana_I': '04b2',  # U+30A4 KATAKANA LETTER I 
    'kana_U': '04b3',  # U+30A6 KATAKANA LETTER U 
    'kana_E': '04b4',  # U+30A8 KATAKANA LETTER E 
    'kana_O': '04b5',  # U+30AA KATAKANA LETTER O 
    'kana_KA': '04b6',  # U+30AB KATAKANA LETTER KA 
    'kana_KI': '04b7',  # U+30AD KATAKANA LETTER KI 
    'kana_KU': '04b8',  # U+30AF KATAKANA LETTER KU 
    'kana_KE': '04b9',  # U+30B1 KATAKANA LETTER KE 
    'kana_KO': '04ba',  # U+30B3 KATAKANA LETTER KO 
    'kana_SA': '04bb',  # U+30B5 KATAKANA LETTER SA 
    'kana_SHI': '04bc',  # U+30B7 KATAKANA LETTER SI 
    'kana_SU': '04bd',  # U+30B9 KATAKANA LETTER SU 
    'kana_SE': '04be',  # U+30BB KATAKANA LETTER SE 
    'kana_SO': '04bf',  # U+30BD KATAKANA LETTER SO 
    'kana_TA': '04c0',  # U+30BF KATAKANA LETTER TA 
    'kana_CHI': '04c1',  # U+30C1 KATAKANA LETTER TI 
    'kana_TI': '04c1',  # deprecated 
    'kana_TSU': '04c2',  # U+30C4 KATAKANA LETTER TU 
    'kana_TU': '04c2',  # deprecated 
    'kana_TE': '04c3',  # U+30C6 KATAKANA LETTER TE 
    'kana_TO': '04c4',  # U+30C8 KATAKANA LETTER TO 
    'kana_NA': '04c5',  # U+30CA KATAKANA LETTER NA 
    'kana_NI': '04c6',  # U+30CB KATAKANA LETTER NI 
    'kana_NU': '04c7',  # U+30CC KATAKANA LETTER NU 
    'kana_NE': '04c8',  # U+30CD KATAKANA LETTER NE 
    'kana_NO': '04c9',  # U+30CE KATAKANA LETTER NO 
    'kana_HA': '04ca',  # U+30CF KATAKANA LETTER HA 
    'kana_HI': '04cb',  # U+30D2 KATAKANA LETTER HI 
    'kana_FU': '04cc',  # U+30D5 KATAKANA LETTER HU 
    'kana_HU': '04cc',  # deprecated 
    'kana_HE': '04cd',  # U+30D8 KATAKANA LETTER HE 
    'kana_HO': '04ce',  # U+30DB KATAKANA LETTER HO 
    'kana_MA': '04cf',  # U+30DE KATAKANA LETTER MA 
    'kana_MI': '04d0',  # U+30DF KATAKANA LETTER MI 
    'kana_MU': '04d1',  # U+30E0 KATAKANA LETTER MU 
    'kana_ME': '04d2',  # U+30E1 KATAKANA LETTER ME 
    'kana_MO': '04d3',  # U+30E2 KATAKANA LETTER MO 
    'kana_YA': '04d4',  # U+30E4 KATAKANA LETTER YA 
    'kana_YU': '04d5',  # U+30E6 KATAKANA LETTER YU 
    'kana_YO': '04d6',  # U+30E8 KATAKANA LETTER YO 
    'kana_RA': '04d7',  # U+30E9 KATAKANA LETTER RA 
    'kana_RI': '04d8',  # U+30EA KATAKANA LETTER RI 
    'kana_RU': '04d9',  # U+30EB KATAKANA LETTER RU 
    'kana_RE': '04da',  # U+30EC KATAKANA LETTER RE 
    'kana_RO': '04db',  # U+30ED KATAKANA LETTER RO 
    'kana_WA': '04dc',  # U+30EF KATAKANA LETTER WA 
    'kana_N': '04dd',  # U+30F3 KATAKANA LETTER N 
    'voicedsound': '04de',  # U+309B KATAKANA-HIRAGANA VOICED SOUND MARK 
    'semivoicedsound': '04df',  # U+309C KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK 
    'kana_switch': 'ff7e',  # Alias for mode_switch 

    # Arabic
    # Byte 3 = 5
    
    'Farsi_0': '10006f0',  # U+06F0 EXTENDED ARABIC-INDIC DIGIT ZERO 
    'Farsi_1': '10006f1',  # U+06F1 EXTENDED ARABIC-INDIC DIGIT ONE 
    'Farsi_2': '10006f2',  # U+06F2 EXTENDED ARABIC-INDIC DIGIT TWO 
    'Farsi_3': '10006f3',  # U+06F3 EXTENDED ARABIC-INDIC DIGIT THREE 
    'Farsi_4': '10006f4',  # U+06F4 EXTENDED ARABIC-INDIC DIGIT FOUR 
    'Farsi_5': '10006f5',  # U+06F5 EXTENDED ARABIC-INDIC DIGIT FIVE 
    'Farsi_6': '10006f6',  # U+06F6 EXTENDED ARABIC-INDIC DIGIT SIX 
    'Farsi_7': '10006f7',  # U+06F7 EXTENDED ARABIC-INDIC DIGIT SEVEN 
    'Farsi_8': '10006f8',  # U+06F8 EXTENDED ARABIC-INDIC DIGIT EIGHT 
    'Farsi_9': '10006f9',  # U+06F9 EXTENDED ARABIC-INDIC DIGIT NINE 
    'Arabic_percent': '100066a',  # U+066A ARABIC PERCENT SIGN 
    'Arabic_superscript_alef': '1000670',  # U+0670 ARABIC LETTER SUPERSCRIPT ALEF 
    'Arabic_tteh': '1000679',  # U+0679 ARABIC LETTER TTEH 
    'Arabic_peh': '100067e',  # U+067E ARABIC LETTER PEH 
    'Arabic_tcheh': '1000686',  # U+0686 ARABIC LETTER TCHEH 
    'Arabic_ddal': '1000688',  # U+0688 ARABIC LETTER DDAL 
    'Arabic_rreh': '1000691',  # U+0691 ARABIC LETTER RREH 
    'Arabic_comma': '05ac',  # U+060C ARABIC COMMA 
    'Arabic_fullstop': '10006d4',  # U+06D4 ARABIC FULL STOP 
    'Arabic_0': '1000660',  # U+0660 ARABIC-INDIC DIGIT ZERO 
    'Arabic_1': '1000661',  # U+0661 ARABIC-INDIC DIGIT ONE 
    'Arabic_2': '1000662',  # U+0662 ARABIC-INDIC DIGIT TWO 
    'Arabic_3': '1000663',  # U+0663 ARABIC-INDIC DIGIT THREE 
    'Arabic_4': '1000664',  # U+0664 ARABIC-INDIC DIGIT FOUR 
    'Arabic_5': '1000665',  # U+0665 ARABIC-INDIC DIGIT FIVE 
    'Arabic_6': '1000666',  # U+0666 ARABIC-INDIC DIGIT SIX 
    'Arabic_7': '1000667',  # U+0667 ARABIC-INDIC DIGIT SEVEN 
    'Arabic_8': '1000668',  # U+0668 ARABIC-INDIC DIGIT EIGHT 
    'Arabic_9': '1000669',  # U+0669 ARABIC-INDIC DIGIT NINE 
    'Arabic_semicolon': '05bb',  # U+061B ARABIC SEMICOLON 
    'Arabic_question_mark': '05bf',  # U+061F ARABIC QUESTION MARK 
    'Arabic_hamza': '05c1',  # U+0621 ARABIC LETTER HAMZA 
    'Arabic_maddaonalef': '05c2',  # U+0622 ARABIC LETTER ALEF WITH MADDA ABOVE 
    'Arabic_hamzaonalef': '05c3',  # U+0623 ARABIC LETTER ALEF WITH HAMZA ABOVE 
    'Arabic_hamzaonwaw': '05c4',  # U+0624 ARABIC LETTER WAW WITH HAMZA ABOVE 
    'Arabic_hamzaunderalef': '05c5',  # U+0625 ARABIC LETTER ALEF WITH HAMZA BELOW 
    'Arabic_hamzaonyeh': '05c6',  # U+0626 ARABIC LETTER YEH WITH HAMZA ABOVE 
    'Arabic_alef': '05c7',  # U+0627 ARABIC LETTER ALEF 
    'Arabic_beh': '05c8',  # U+0628 ARABIC LETTER BEH 
    'Arabic_tehmarbuta': '05c9',  # U+0629 ARABIC LETTER TEH MARBUTA 
    'Arabic_teh': '05ca',  # U+062A ARABIC LETTER TEH 
    'Arabic_theh': '05cb',  # U+062B ARABIC LETTER THEH 
    'Arabic_jeem': '05cc',  # U+062C ARABIC LETTER JEEM 
    'Arabic_hah': '05cd',  # U+062D ARABIC LETTER HAH 
    'Arabic_khah': '05ce',  # U+062E ARABIC LETTER KHAH 
    'Arabic_dal': '05cf',  # U+062F ARABIC LETTER DAL 
    'Arabic_thal': '05d0',  # U+0630 ARABIC LETTER THAL 
    'Arabic_ra': '05d1',  # U+0631 ARABIC LETTER REH 
    'Arabic_zain': '05d2',  # U+0632 ARABIC LETTER ZAIN 
    'Arabic_seen': '05d3',  # U+0633 ARABIC LETTER SEEN 
    'Arabic_sheen': '05d4',  # U+0634 ARABIC LETTER SHEEN 
    'Arabic_sad': '05d5',  # U+0635 ARABIC LETTER SAD 
    'Arabic_dad': '05d6',  # U+0636 ARABIC LETTER DAD 
    'Arabic_tah': '05d7',  # U+0637 ARABIC LETTER TAH 
    'Arabic_zah': '05d8',  # U+0638 ARABIC LETTER ZAH 
    'Arabic_ain': '05d9',  # U+0639 ARABIC LETTER AIN 
    'Arabic_ghain': '05da',  # U+063A ARABIC LETTER GHAIN 
    'Arabic_tatweel': '05e0',  # U+0640 ARABIC TATWEEL 
    'Arabic_feh': '05e1',  # U+0641 ARABIC LETTER FEH 
    'Arabic_qaf': '05e2',  # U+0642 ARABIC LETTER QAF 
    'Arabic_kaf': '05e3',  # U+0643 ARABIC LETTER KAF 
    'Arabic_lam': '05e4',  # U+0644 ARABIC LETTER LAM 
    'Arabic_meem': '05e5',  # U+0645 ARABIC LETTER MEEM 
    'Arabic_noon': '05e6',  # U+0646 ARABIC LETTER NOON 
    'Arabic_ha': '05e7',  # U+0647 ARABIC LETTER HEH 
    'Arabic_heh': '05e7',  # deprecated 
    'Arabic_waw': '05e8',  # U+0648 ARABIC LETTER WAW 
    'Arabic_alefmaksura': '05e9',  # U+0649 ARABIC LETTER ALEF MAKSURA 
    'Arabic_yeh': '05ea',  # U+064A ARABIC LETTER YEH 
    'Arabic_fathatan': '05eb',  # U+064B ARABIC FATHATAN 
    'Arabic_dammatan': '05ec',  # U+064C ARABIC DAMMATAN 
    'Arabic_kasratan': '05ed',  # U+064D ARABIC KASRATAN 
    'Arabic_fatha': '05ee',  # U+064E ARABIC FATHA 
    'Arabic_damma': '05ef',  # U+064F ARABIC DAMMA 
    'Arabic_kasra': '05f0',  # U+0650 ARABIC KASRA 
    'Arabic_shadda': '05f1',  # U+0651 ARABIC SHADDA 
    'Arabic_sukun': '05f2',  # U+0652 ARABIC SUKUN 
    'Arabic_madda_above': '1000653',  # U+0653 ARABIC MADDAH ABOVE 
    'Arabic_hamza_above': '1000654',  # U+0654 ARABIC HAMZA ABOVE 
    'Arabic_hamza_below': '1000655',  # U+0655 ARABIC HAMZA BELOW 
    'Arabic_jeh': '1000698',  # U+0698 ARABIC LETTER JEH 
    'Arabic_veh': '10006a4',  # U+06A4 ARABIC LETTER VEH 
    'Arabic_keheh': '10006a9',  # U+06A9 ARABIC LETTER KEHEH 
    'Arabic_gaf': '10006af',  # U+06AF ARABIC LETTER GAF 
    'Arabic_noon_ghunna': '10006ba',  # U+06BA ARABIC LETTER NOON GHUNNA 
    'Arabic_heh_doachashmee': '10006be',  # U+06BE ARABIC LETTER HEH DOACHASHMEE 
    'Farsi_yeh': '10006cc',  # U+06CC ARABIC LETTER FARSI YEH 
    'Arabic_farsi_yeh': '10006cc',  # U+06CC ARABIC LETTER FARSI YEH 
    'Arabic_yeh_baree': '10006d2',  # U+06D2 ARABIC LETTER YEH BARREE 
    'Arabic_heh_goal': '10006c1',  # U+06C1 ARABIC LETTER HEH GOAL 
    'Arabic_switch': 'ff7e',  # Alias for mode_switch 

    # Cyrillic
    # Byte 3 = 6
    
    'Cyrillic_GHE_bar': '1000492',  # U+0492 CYRILLIC CAPITAL LETTER GHE WITH STROKE 
    'Cyrillic_ghe_bar': '1000493',  # U+0493 CYRILLIC SMALL LETTER GHE WITH STROKE 
    'Cyrillic_ZHE_descender': '1000496',  # U+0496 CYRILLIC CAPITAL LETTER ZHE WITH DESCENDER 
    'Cyrillic_zhe_descender': '1000497',  # U+0497 CYRILLIC SMALL LETTER ZHE WITH DESCENDER 
    'Cyrillic_KA_descender': '100049a',  # U+049A CYRILLIC CAPITAL LETTER KA WITH DESCENDER 
    'Cyrillic_ka_descender': '100049b',  # U+049B CYRILLIC SMALL LETTER KA WITH DESCENDER 
    'Cyrillic_KA_vertstroke': '100049c',  # U+049C CYRILLIC CAPITAL LETTER KA WITH VERTICAL STROKE 
    'Cyrillic_ka_vertstroke': '100049d',  # U+049D CYRILLIC SMALL LETTER KA WITH VERTICAL STROKE 
    'Cyrillic_EN_descender': '10004a2',  # U+04A2 CYRILLIC CAPITAL LETTER EN WITH DESCENDER 
    'Cyrillic_en_descender': '10004a3',  # U+04A3 CYRILLIC SMALL LETTER EN WITH DESCENDER 
    'Cyrillic_U_straight': '10004ae',  # U+04AE CYRILLIC CAPITAL LETTER STRAIGHT U 
    'Cyrillic_u_straight': '10004af',  # U+04AF CYRILLIC SMALL LETTER STRAIGHT U 
    'Cyrillic_U_straight_bar': '10004b0',  # U+04B0 CYRILLIC CAPITAL LETTER STRAIGHT U WITH STROKE 
    'Cyrillic_u_straight_bar': '10004b1',  # U+04B1 CYRILLIC SMALL LETTER STRAIGHT U WITH STROKE 
    'Cyrillic_HA_descender': '10004b2',  # U+04B2 CYRILLIC CAPITAL LETTER HA WITH DESCENDER 
    'Cyrillic_ha_descender': '10004b3',  # U+04B3 CYRILLIC SMALL LETTER HA WITH DESCENDER 
    'Cyrillic_CHE_descender': '10004b6',  # U+04B6 CYRILLIC CAPITAL LETTER CHE WITH DESCENDER 
    'Cyrillic_che_descender': '10004b7',  # U+04B7 CYRILLIC SMALL LETTER CHE WITH DESCENDER 
    'Cyrillic_CHE_vertstroke': '10004b8',  # U+04B8 CYRILLIC CAPITAL LETTER CHE WITH VERTICAL STROKE 
    'Cyrillic_che_vertstroke': '10004b9',  # U+04B9 CYRILLIC SMALL LETTER CHE WITH VERTICAL STROKE 
    'Cyrillic_SHHA': '10004ba',  # U+04BA CYRILLIC CAPITAL LETTER SHHA 
    'Cyrillic_shha': '10004bb',  # U+04BB CYRILLIC SMALL LETTER SHHA 

    'Cyrillic_SCHWA': '10004d8',  # U+04D8 CYRILLIC CAPITAL LETTER SCHWA 
    'Cyrillic_schwa': '10004d9',  # U+04D9 CYRILLIC SMALL LETTER SCHWA 
    'Cyrillic_I_macron': '10004e2',  # U+04E2 CYRILLIC CAPITAL LETTER I WITH MACRON 
    'Cyrillic_i_macron': '10004e3',  # U+04E3 CYRILLIC SMALL LETTER I WITH MACRON 
    'Cyrillic_O_bar': '10004e8',  # U+04E8 CYRILLIC CAPITAL LETTER BARRED O 
    'Cyrillic_o_bar': '10004e9',  # U+04E9 CYRILLIC SMALL LETTER BARRED O 
    'Cyrillic_U_macron': '10004ee',  # U+04EE CYRILLIC CAPITAL LETTER U WITH MACRON 
    'Cyrillic_u_macron': '10004ef',  # U+04EF CYRILLIC SMALL LETTER U WITH MACRON 

    'Serbian_dje': '06a1',  # U+0452 CYRILLIC SMALL LETTER DJE 
    'Macedonia_gje': '06a2',  # U+0453 CYRILLIC SMALL LETTER GJE 
    'Cyrillic_io': '06a3',  # U+0451 CYRILLIC SMALL LETTER IO 
    'Ukrainian_ie': '06a4',  # U+0454 CYRILLIC SMALL LETTER UKRAINIAN IE 
    'Ukranian_je': '06a4',  # deprecated 
    'Macedonia_dse': '06a5',  # U+0455 CYRILLIC SMALL LETTER DZE 
    'Ukrainian_i': '06a6',  # U+0456 CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I 
    'Ukranian_i': '06a6',  # deprecated 
    'Ukrainian_yi': '06a7',  # U+0457 CYRILLIC SMALL LETTER YI 
    'Ukranian_yi': '06a7',  # deprecated 
    'Cyrillic_je': '06a8',  # U+0458 CYRILLIC SMALL LETTER JE 
    'Serbian_je': '06a8',  # deprecated 
    'Cyrillic_lje': '06a9',  # U+0459 CYRILLIC SMALL LETTER LJE 
    'Serbian_lje': '06a9',  # deprecated 
    'Cyrillic_nje': '06aa',  # U+045A CYRILLIC SMALL LETTER NJE 
    'Serbian_nje': '06aa',  # deprecated 
    'Serbian_tshe': '06ab',  # U+045B CYRILLIC SMALL LETTER TSHE 
    'Macedonia_kje': '06ac',  # U+045C CYRILLIC SMALL LETTER KJE 
    'Ukrainian_ghe_with_upturn': '06ad',  # U+0491 CYRILLIC SMALL LETTER GHE WITH UPTURN 
    'Byelorussian_shortu': '06ae',  # U+045E CYRILLIC SMALL LETTER SHORT U 
    'Cyrillic_dzhe': '06af',  # U+045F CYRILLIC SMALL LETTER DZHE 
    'Serbian_dze': '06af',  # deprecated 
    'numerosign': '06b0',  # U+2116 NUMERO SIGN 
    'Serbian_DJE': '06b1',  # U+0402 CYRILLIC CAPITAL LETTER DJE 
    'Macedonia_GJE': '06b2',  # U+0403 CYRILLIC CAPITAL LETTER GJE 
    'Cyrillic_IO': '06b3',  # U+0401 CYRILLIC CAPITAL LETTER IO 
    'Ukrainian_IE': '06b4',  # U+0404 CYRILLIC CAPITAL LETTER UKRAINIAN IE 
    'Ukranian_JE': '06b4',  # deprecated 
    'Macedonia_DSE': '06b5',  # U+0405 CYRILLIC CAPITAL LETTER DZE 
    'Ukrainian_I': '06b6',  # U+0406 CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I 
    'Ukranian_I': '06b6',  # deprecated 
    'Ukrainian_YI': '06b7',  # U+0407 CYRILLIC CAPITAL LETTER YI 
    'Ukranian_YI': '06b7',  # deprecated 
    'Cyrillic_JE': '06b8',  # U+0408 CYRILLIC CAPITAL LETTER JE 
    'Serbian_JE': '06b8',  # deprecated 
    'Cyrillic_LJE': '06b9',  # U+0409 CYRILLIC CAPITAL LETTER LJE 
    'Serbian_LJE': '06b9',  # deprecated 
    'Cyrillic_NJE': '06ba',  # U+040A CYRILLIC CAPITAL LETTER NJE 
    'Serbian_NJE': '06ba',  # deprecated 
    'Serbian_TSHE': '06bb',  # U+040B CYRILLIC CAPITAL LETTER TSHE 
    'Macedonia_KJE': '06bc',  # U+040C CYRILLIC CAPITAL LETTER KJE 
    'Ukrainian_GHE_WITH_UPTURN': '06bd',  # U+0490 CYRILLIC CAPITAL LETTER GHE WITH UPTURN 
    'Byelorussian_SHORTU': '06be',  # U+040E CYRILLIC CAPITAL LETTER SHORT U 
    'Cyrillic_DZHE': '06bf',  # U+040F CYRILLIC CAPITAL LETTER DZHE 
    'Serbian_DZE': '06bf',  # deprecated 
    'Cyrillic_yu': '06c0',  # U+044E CYRILLIC SMALL LETTER YU 
    'Cyrillic_a': '06c1',  # U+0430 CYRILLIC SMALL LETTER A 
    'Cyrillic_be': '06c2',  # U+0431 CYRILLIC SMALL LETTER BE 
    'Cyrillic_tse': '06c3',  # U+0446 CYRILLIC SMALL LETTER TSE 
    'Cyrillic_de': '06c4',  # U+0434 CYRILLIC SMALL LETTER DE 
    'Cyrillic_ie': '06c5',  # U+0435 CYRILLIC SMALL LETTER IE 
    'Cyrillic_ef': '06c6',  # U+0444 CYRILLIC SMALL LETTER EF 
    'Cyrillic_ghe': '06c7',  # U+0433 CYRILLIC SMALL LETTER GHE 
    'Cyrillic_ha': '06c8',  # U+0445 CYRILLIC SMALL LETTER HA 
    'Cyrillic_i': '06c9',  # U+0438 CYRILLIC SMALL LETTER I 
    'Cyrillic_shorti': '06ca',  # U+0439 CYRILLIC SMALL LETTER SHORT I 
    'Cyrillic_ka': '06cb',  # U+043A CYRILLIC SMALL LETTER KA 
    'Cyrillic_el': '06cc',  # U+043B CYRILLIC SMALL LETTER EL 
    'Cyrillic_em': '06cd',  # U+043C CYRILLIC SMALL LETTER EM 
    'Cyrillic_en': '06ce',  # U+043D CYRILLIC SMALL LETTER EN 
    'Cyrillic_o': '06cf',  # U+043E CYRILLIC SMALL LETTER O 
    'Cyrillic_pe': '06d0',  # U+043F CYRILLIC SMALL LETTER PE 
    'Cyrillic_ya': '06d1',  # U+044F CYRILLIC SMALL LETTER YA 
    'Cyrillic_er': '06d2',  # U+0440 CYRILLIC SMALL LETTER ER 
    'Cyrillic_es': '06d3',  # U+0441 CYRILLIC SMALL LETTER ES 
    'Cyrillic_te': '06d4',  # U+0442 CYRILLIC SMALL LETTER TE 
    'Cyrillic_u': '06d5',  # U+0443 CYRILLIC SMALL LETTER U 
    'Cyrillic_zhe': '06d6',  # U+0436 CYRILLIC SMALL LETTER ZHE 
    'Cyrillic_ve': '06d7',  # U+0432 CYRILLIC SMALL LETTER VE 
    'Cyrillic_softsign': '06d8',  # U+044C CYRILLIC SMALL LETTER SOFT SIGN 
    'Cyrillic_yeru': '06d9',  # U+044B CYRILLIC SMALL LETTER YERU 
    'Cyrillic_ze': '06da',  # U+0437 CYRILLIC SMALL LETTER ZE 
    'Cyrillic_sha': '06db',  # U+0448 CYRILLIC SMALL LETTER SHA 
    'Cyrillic_e': '06dc',  # U+044D CYRILLIC SMALL LETTER E 
    'Cyrillic_shcha': '06dd',  # U+0449 CYRILLIC SMALL LETTER SHCHA 
    'Cyrillic_che': '06de',  # U+0447 CYRILLIC SMALL LETTER CHE 
    'Cyrillic_hardsign': '06df',  # U+044A CYRILLIC SMALL LETTER HARD SIGN 
    'Cyrillic_YU': '06e0',  # U+042E CYRILLIC CAPITAL LETTER YU 
    'Cyrillic_A': '06e1',  # U+0410 CYRILLIC CAPITAL LETTER A 
    'Cyrillic_BE': '06e2',  # U+0411 CYRILLIC CAPITAL LETTER BE 
    'Cyrillic_TSE': '06e3',  # U+0426 CYRILLIC CAPITAL LETTER TSE 
    'Cyrillic_DE': '06e4',  # U+0414 CYRILLIC CAPITAL LETTER DE 
    'Cyrillic_IE': '06e5',  # U+0415 CYRILLIC CAPITAL LETTER IE 
    'Cyrillic_EF': '06e6',  # U+0424 CYRILLIC CAPITAL LETTER EF 
    'Cyrillic_GHE': '06e7',  # U+0413 CYRILLIC CAPITAL LETTER GHE 
    'Cyrillic_HA': '06e8',  # U+0425 CYRILLIC CAPITAL LETTER HA 
    'Cyrillic_I': '06e9',  # U+0418 CYRILLIC CAPITAL LETTER I 
    'Cyrillic_SHORTI': '06ea',  # U+0419 CYRILLIC CAPITAL LETTER SHORT I 
    'Cyrillic_KA': '06eb',  # U+041A CYRILLIC CAPITAL LETTER KA 
    'Cyrillic_EL': '06ec',  # U+041B CYRILLIC CAPITAL LETTER EL 
    'Cyrillic_EM': '06ed',  # U+041C CYRILLIC CAPITAL LETTER EM 
    'Cyrillic_EN': '06ee',  # U+041D CYRILLIC CAPITAL LETTER EN 
    'Cyrillic_O': '06ef',  # U+041E CYRILLIC CAPITAL LETTER O 
    'Cyrillic_PE': '06f0',  # U+041F CYRILLIC CAPITAL LETTER PE 
    'Cyrillic_YA': '06f1',  # U+042F CYRILLIC CAPITAL LETTER YA 
    'Cyrillic_ER': '06f2',  # U+0420 CYRILLIC CAPITAL LETTER ER 
    'Cyrillic_ES': '06f3',  # U+0421 CYRILLIC CAPITAL LETTER ES 
    'Cyrillic_TE': '06f4',  # U+0422 CYRILLIC CAPITAL LETTER TE 
    'Cyrillic_U': '06f5',  # U+0423 CYRILLIC CAPITAL LETTER U 
    'Cyrillic_ZHE': '06f6',  # U+0416 CYRILLIC CAPITAL LETTER ZHE 
    'Cyrillic_VE': '06f7',  # U+0412 CYRILLIC CAPITAL LETTER VE 
    'Cyrillic_SOFTSIGN': '06f8',  # U+042C CYRILLIC CAPITAL LETTER SOFT SIGN 
    'Cyrillic_YERU': '06f9',  # U+042B CYRILLIC CAPITAL LETTER YERU 
    'Cyrillic_ZE': '06fa',  # U+0417 CYRILLIC CAPITAL LETTER ZE 
    'Cyrillic_SHA': '06fb',  # U+0428 CYRILLIC CAPITAL LETTER SHA 
    'Cyrillic_E': '06fc',  # U+042D CYRILLIC CAPITAL LETTER E 
    'Cyrillic_SHCHA': '06fd',  # U+0429 CYRILLIC CAPITAL LETTER SHCHA 
    'Cyrillic_CHE': '06fe',  # U+0427 CYRILLIC CAPITAL LETTER CHE 
    'Cyrillic_HARDSIGN': '06ff',  # U+042A CYRILLIC CAPITAL LETTER HARD SIGN 

    # Greek
    # (based on an early draft of, and not quite identical to, ISO/IEC 8859-7)
    # Byte 3 = 7
    
    'Greek_ALPHAaccent': '07a1',  # U+0386 GREEK CAPITAL LETTER ALPHA WITH TONOS 
    'Greek_EPSILONaccent': '07a2',  # U+0388 GREEK CAPITAL LETTER EPSILON WITH TONOS 
    'Greek_ETAaccent': '07a3',  # U+0389 GREEK CAPITAL LETTER ETA WITH TONOS 
    'Greek_IOTAaccent': '07a4',  # U+038A GREEK CAPITAL LETTER IOTA WITH TONOS 
    'Greek_IOTAdieresis': '07a5',  # U+03AA GREEK CAPITAL LETTER IOTA WITH DIALYTIKA 
    'Greek_IOTAdiaeresis': '07a5',  # old typo 
    'Greek_OMICRONaccent': '07a7',  # U+038C GREEK CAPITAL LETTER OMICRON WITH TONOS 
    'Greek_UPSILONaccent': '07a8',  # U+038E GREEK CAPITAL LETTER UPSILON WITH TONOS 
    'Greek_UPSILONdieresis': '07a9',  # U+03AB GREEK CAPITAL LETTER UPSILON WITH DIALYTIKA 
    'Greek_OMEGAaccent': '07ab',  # U+038F GREEK CAPITAL LETTER OMEGA WITH TONOS 
    'Greek_accentdieresis': '07ae',  # U+0385 GREEK DIALYTIKA TONOS 
    'Greek_horizbar': '07af',  # U+2015 HORIZONTAL BAR 
    'Greek_alphaaccent': '07b1',  # U+03AC GREEK SMALL LETTER ALPHA WITH TONOS 
    'Greek_epsilonaccent': '07b2',  # U+03AD GREEK SMALL LETTER EPSILON WITH TONOS 
    'Greek_etaaccent': '07b3',  # U+03AE GREEK SMALL LETTER ETA WITH TONOS 
    'Greek_iotaaccent': '07b4',  # U+03AF GREEK SMALL LETTER IOTA WITH TONOS 
    'Greek_iotadieresis': '07b5',  # U+03CA GREEK SMALL LETTER IOTA WITH DIALYTIKA 
    'Greek_iotaaccentdieresis': '07b6',  # U+0390 GREEK SMALL LETTER IOTA WITH DIALYTIKA AND TONOS 
    'Greek_omicronaccent': '07b7',  # U+03CC GREEK SMALL LETTER OMICRON WITH TONOS 
    'Greek_upsilonaccent': '07b8',  # U+03CD GREEK SMALL LETTER UPSILON WITH TONOS 
    'Greek_upsilondieresis': '07b9',  # U+03CB GREEK SMALL LETTER UPSILON WITH DIALYTIKA 
    'Greek_upsilonaccentdieresis': '07ba',  # U+03B0 GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS 
    'Greek_omegaaccent': '07bb',  # U+03CE GREEK SMALL LETTER OMEGA WITH TONOS 
    'Greek_ALPHA': '07c1',  # U+0391 GREEK CAPITAL LETTER ALPHA 
    'Greek_BETA': '07c2',  # U+0392 GREEK CAPITAL LETTER BETA 
    'Greek_GAMMA': '07c3',  # U+0393 GREEK CAPITAL LETTER GAMMA 
    'Greek_DELTA': '07c4',  # U+0394 GREEK CAPITAL LETTER DELTA 
    'Greek_EPSILON': '07c5',  # U+0395 GREEK CAPITAL LETTER EPSILON 
    'Greek_ZETA': '07c6',  # U+0396 GREEK CAPITAL LETTER ZETA 
    'Greek_ETA': '07c7',  # U+0397 GREEK CAPITAL LETTER ETA 
    'Greek_THETA': '07c8',  # U+0398 GREEK CAPITAL LETTER THETA 
    'Greek_IOTA': '07c9',  # U+0399 GREEK CAPITAL LETTER IOTA 
    'Greek_KAPPA': '07ca',  # U+039A GREEK CAPITAL LETTER KAPPA 
    'Greek_LAMDA': '07cb',  # U+039B GREEK CAPITAL LETTER LAMDA 
    'Greek_LAMBDA': '07cb',  # U+039B GREEK CAPITAL LETTER LAMDA 
    'Greek_MU': '07cc',  # U+039C GREEK CAPITAL LETTER MU 
    'Greek_NU': '07cd',  # U+039D GREEK CAPITAL LETTER NU 
    'Greek_XI': '07ce',  # U+039E GREEK CAPITAL LETTER XI 
    'Greek_OMICRON': '07cf',  # U+039F GREEK CAPITAL LETTER OMICRON 
    'Greek_PI': '07d0',  # U+03A0 GREEK CAPITAL LETTER PI 
    'Greek_RHO': '07d1',  # U+03A1 GREEK CAPITAL LETTER RHO 
    'Greek_SIGMA': '07d2',  # U+03A3 GREEK CAPITAL LETTER SIGMA 
    'Greek_TAU': '07d4',  # U+03A4 GREEK CAPITAL LETTER TAU 
    'Greek_UPSILON': '07d5',  # U+03A5 GREEK CAPITAL LETTER UPSILON 
    'Greek_PHI': '07d6',  # U+03A6 GREEK CAPITAL LETTER PHI 
    'Greek_CHI': '07d7',  # U+03A7 GREEK CAPITAL LETTER CHI 
    'Greek_PSI': '07d8',  # U+03A8 GREEK CAPITAL LETTER PSI 
    'Greek_OMEGA': '07d9',  # U+03A9 GREEK CAPITAL LETTER OMEGA 
    'Greek_alpha': '07e1',  # U+03B1 GREEK SMALL LETTER ALPHA 
    'Greek_beta': '07e2',  # U+03B2 GREEK SMALL LETTER BETA 
    'Greek_gamma': '07e3',  # U+03B3 GREEK SMALL LETTER GAMMA 
    'Greek_delta': '07e4',  # U+03B4 GREEK SMALL LETTER DELTA 
    'Greek_epsilon': '07e5',  # U+03B5 GREEK SMALL LETTER EPSILON 
    'Greek_zeta': '07e6',  # U+03B6 GREEK SMALL LETTER ZETA 
    'Greek_eta': '07e7',  # U+03B7 GREEK SMALL LETTER ETA 
    'Greek_theta': '07e8',  # U+03B8 GREEK SMALL LETTER THETA 
    'Greek_iota': '07e9',  # U+03B9 GREEK SMALL LETTER IOTA 
    'Greek_kappa': '07ea',  # U+03BA GREEK SMALL LETTER KAPPA 
    'Greek_lamda': '07eb',  # U+03BB GREEK SMALL LETTER LAMDA 
    'Greek_lambda': '07eb',  # U+03BB GREEK SMALL LETTER LAMDA 
    'Greek_mu': '07ec',  # U+03BC GREEK SMALL LETTER MU 
    'Greek_nu': '07ed',  # U+03BD GREEK SMALL LETTER NU 
    'Greek_xi': '07ee',  # U+03BE GREEK SMALL LETTER XI 
    'Greek_omicron': '07ef',  # U+03BF GREEK SMALL LETTER OMICRON 
    'Greek_pi': '07f0',  # U+03C0 GREEK SMALL LETTER PI 
    'Greek_rho': '07f1',  # U+03C1 GREEK SMALL LETTER RHO 
    'Greek_sigma': '07f2',  # U+03C3 GREEK SMALL LETTER SIGMA 
    'Greek_finalsmallsigma': '07f3',  # U+03C2 GREEK SMALL LETTER FINAL SIGMA 
    'Greek_tau': '07f4',  # U+03C4 GREEK SMALL LETTER TAU 
    'Greek_upsilon': '07f5',  # U+03C5 GREEK SMALL LETTER UPSILON 
    'Greek_phi': '07f6',  # U+03C6 GREEK SMALL LETTER PHI 
    'Greek_chi': '07f7',  # U+03C7 GREEK SMALL LETTER CHI 
    'Greek_psi': '07f8',  # U+03C8 GREEK SMALL LETTER PSI 
    'Greek_omega': '07f9',  # U+03C9 GREEK SMALL LETTER OMEGA 
    'Greek_switch': 'ff7e',  # Alias for mode_switch 

    # Technical
    # (from the DEC VT330/VT420 Technical Character Set, http://vt100.net/charsets/technical.html)
    # Byte 3 = 8
    
    'leftradical': '08a1',  # U+23B7 RADICAL SYMBOL BOTTOM 
    'topleftradical': '08a2',  #(U+250C BOX DRAWINGS LIGHT DOWN AND RIGHT)
    'horizconnector': '08a3',  #(U+2500 BOX DRAWINGS LIGHT HORIZONTAL)
    'topintegral': '08a4',  # U+2320 TOP HALF INTEGRAL 
    'botintegral': '08a5',  # U+2321 BOTTOM HALF INTEGRAL 
    'vertconnector': '08a6',  #(U+2502 BOX DRAWINGS LIGHT VERTICAL)
    'topleftsqbracket': '08a7',  # U+23A1 LEFT SQUARE BRACKET UPPER CORNER 
    'botleftsqbracket': '08a8',  # U+23A3 LEFT SQUARE BRACKET LOWER CORNER 
    'toprightsqbracket': '08a9',  # U+23A4 RIGHT SQUARE BRACKET UPPER CORNER 
    'botrightsqbracket': '08aa',  # U+23A6 RIGHT SQUARE BRACKET LOWER CORNER 
    'topleftparens': '08ab',  # U+239B LEFT PARENTHESIS UPPER HOOK 
    'botleftparens': '08ac',  # U+239D LEFT PARENTHESIS LOWER HOOK 
    'toprightparens': '08ad',  # U+239E RIGHT PARENTHESIS UPPER HOOK 
    'botrightparens': '08ae',  # U+23A0 RIGHT PARENTHESIS LOWER HOOK 
    'leftmiddlecurlybrace': '08af',  # U+23A8 LEFT CURLY BRACKET MIDDLE PIECE 
    'rightmiddlecurlybrace': '08b0',  # U+23AC RIGHT CURLY BRACKET MIDDLE PIECE 
    'topleftsummation': '08b1',
    'botleftsummation': '08b2',
    'topvertsummationconnector': '08b3',
    'botvertsummationconnector': '08b4',
    'toprightsummation': '08b5',
    'botrightsummation': '08b6',
    'rightmiddlesummation': '08b7',
    'lessthanequal': '08bc',  # U+2264 LESS-THAN OR EQUAL TO 
    'notequal': '08bd',  # U+2260 NOT EQUAL TO 
    'greaterthanequal': '08be',  # U+2265 GREATER-THAN OR EQUAL TO 
    'integral': '08bf',  # U+222B INTEGRAL 
    'therefore': '08c0',  # U+2234 THEREFORE 
    'variation': '08c1',  # U+221D PROPORTIONAL TO 
    'infinity': '08c2',  # U+221E INFINITY 
    'nabla': '08c5',  # U+2207 NABLA 
    'approximate': '08c8',  # U+223C TILDE OPERATOR 
    'similarequal': '08c9',  # U+2243 ASYMPTOTICALLY EQUAL TO 
    'ifonlyif': '08cd',  # U+21D4 LEFT RIGHT DOUBLE ARROW 
    'implies': '08ce',  # U+21D2 RIGHTWARDS DOUBLE ARROW 
    'identical': '08cf',  # U+2261 IDENTICAL TO 
    'radical': '08d6',  # U+221A SQUARE ROOT 
    'includedin': '08da',  # U+2282 SUBSET OF 
    'includes': '08db',  # U+2283 SUPERSET OF 
    'intersection': '08dc',  # U+2229 INTERSECTION 
    'union': '08dd',  # U+222A UNION 
    'logicaland': '08de',  # U+2227 LOGICAL AND 
    'logicalor': '08df',  # U+2228 LOGICAL OR 
    'partialderivative': '08ef',  # U+2202 PARTIAL DIFFERENTIAL 
    'function': '08f6',  # U+0192 LATIN SMALL LETTER F WITH HOOK 
    'leftarrow': '08fb',  # U+2190 LEFTWARDS ARROW 
    'uparrow': '08fc',  # U+2191 UPWARDS ARROW 
    'rightarrow': '08fd',  # U+2192 RIGHTWARDS ARROW 
    'downarrow': '08fe',  # U+2193 DOWNWARDS ARROW 

    # Special
    # (from the DEC VT100 Special Graphics Character Set)
    # Byte 3 = 9
    
    'blank': '09df',
    'soliddiamond': '09e0',  # U+25C6 BLACK DIAMOND 
    'checkerboard': '09e1',  # U+2592 MEDIUM SHADE 
    'ht': '09e2',  # U+2409 SYMBOL FOR HORIZONTAL TABULATION 
    'ff': '09e3',  # U+240C SYMBOL FOR FORM FEED 
    'cr': '09e4',  # U+240D SYMBOL FOR CARRIAGE RETURN 
    'lf': '09e5',  # U+240A SYMBOL FOR LINE FEED 
    'nl': '09e8',  # U+2424 SYMBOL FOR NEWLINE 
    'vt': '09e9',  # U+240B SYMBOL FOR VERTICAL TABULATION 
    'lowrightcorner': '09ea',  # U+2518 BOX DRAWINGS LIGHT UP AND LEFT 
    'uprightcorner': '09eb',  # U+2510 BOX DRAWINGS LIGHT DOWN AND LEFT 
    'upleftcorner': '09ec',  # U+250C BOX DRAWINGS LIGHT DOWN AND RIGHT 
    'lowleftcorner': '09ed',  # U+2514 BOX DRAWINGS LIGHT UP AND RIGHT 
    'crossinglines': '09ee',  # U+253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL 
    'horizlinescan1': '09ef',  # U+23BA HORIZONTAL SCAN LINE-1 
    'horizlinescan3': '09f0',  # U+23BB HORIZONTAL SCAN LINE-3 
    'horizlinescan5': '09f1',  # U+2500 BOX DRAWINGS LIGHT HORIZONTAL 
    'horizlinescan7': '09f2',  # U+23BC HORIZONTAL SCAN LINE-7 
    'horizlinescan9': '09f3',  # U+23BD HORIZONTAL SCAN LINE-9 
    'leftt': '09f4',  # U+251C BOX DRAWINGS LIGHT VERTICAL AND RIGHT 
    'rightt': '09f5',  # U+2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT 
    'bott': '09f6',  # U+2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL 
    'topt': '09f7',  # U+252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL 
    'vertbar': '09f8',  # U+2502 BOX DRAWINGS LIGHT VERTICAL 

    # Publishing
    # (these are probably from a long forgotten DEC Publishing
    # font that once shipped with DECwrite)
    # Byte 3 =': '0a
    
    'emspace': '0aa1',  # U+2003 EM SPACE 
    'enspace': '0aa2',  # U+2002 EN SPACE 
    'em3space': '0aa3',  # U+2004 THREE-PER-EM SPACE 
    'em4space': '0aa4',  # U+2005 FOUR-PER-EM SPACE 
    'digitspace': '0aa5',  # U+2007 FIGURE SPACE 
    'punctspace': '0aa6',  # U+2008 PUNCTUATION SPACE 
    'thinspace': '0aa7',  # U+2009 THIN SPACE 
    'hairspace': '0aa8',  # U+200A HAIR SPACE 
    'emdash': '0aa9',  # U+2014 EM DASH 
    'endash': '0aaa',  # U+2013 EN DASH 
    'signifblank': '0aac',  #(U+2423 OPEN BOX)
    'ellipsis': '0aae',  # U+2026 HORIZONTAL ELLIPSIS 
    'doubbaselinedot': '0aaf',  # U+2025 TWO DOT LEADER 
    'onethird': '0ab0',  # U+2153 VULGAR FRACTION ONE THIRD 
    'twothirds': '0ab1',  # U+2154 VULGAR FRACTION TWO THIRDS 
    'onefifth': '0ab2',  # U+2155 VULGAR FRACTION ONE FIFTH 
    'twofifths': '0ab3',  # U+2156 VULGAR FRACTION TWO FIFTHS 
    'threefifths': '0ab4',  # U+2157 VULGAR FRACTION THREE FIFTHS 
    'fourfifths': '0ab5',  # U+2158 VULGAR FRACTION FOUR FIFTHS 
    'onesixth': '0ab6',  # U+2159 VULGAR FRACTION ONE SIXTH 
    'fivesixths': '0ab7',  # U+215A VULGAR FRACTION FIVE SIXTHS 
    'careof': '0ab8',  # U+2105 CARE OF 
    'figdash': '0abb',  # U+2012 FIGURE DASH 
    'leftanglebracket': '0abc',  #(U+27E8 MATHEMATICAL LEFT ANGLE BRACKET)
    'decimalpoint': '0abd',  #(U+002E FULL STOP)
    'rightanglebracket': '0abe',  #(U+27E9 MATHEMATICAL RIGHT ANGLE BRACKET)
    'marker': '0abf',
    'oneeighth': '0ac3',  # U+215B VULGAR FRACTION ONE EIGHTH 
    'threeeighths': '0ac4',  # U+215C VULGAR FRACTION THREE EIGHTHS 
    'fiveeighths': '0ac5',  # U+215D VULGAR FRACTION FIVE EIGHTHS 
    'seveneighths': '0ac6',  # U+215E VULGAR FRACTION SEVEN EIGHTHS 
    'trademark': '0ac9',  # U+2122 TRADE MARK SIGN 
    'signaturemark': '0aca',  #(U+2613 SALTIRE)
    'trademarkincircle': '0acb',
    'leftopentriangle': '0acc',  #(U+25C1 WHITE LEFT-POINTING TRIANGLE)
    'rightopentriangle': '0acd',  #(U+25B7 WHITE RIGHT-POINTING TRIANGLE)
    'emopencircle': '0ace',  #(U+25CB WHITE CIRCLE)
    'emopenrectangle': '0acf',  #(U+25AF WHITE VERTICAL RECTANGLE)
    'leftsinglequotemark': '0ad0',  # U+2018 LEFT SINGLE QUOTATION MARK 
    'rightsinglequotemark': '0ad1',  # U+2019 RIGHT SINGLE QUOTATION MARK 
    'leftdoublequotemark': '0ad2',  # U+201C LEFT DOUBLE QUOTATION MARK 
    'rightdoublequotemark': '0ad3',  # U+201D RIGHT DOUBLE QUOTATION MARK 
    'prescription': '0ad4',  # U+211E PRESCRIPTION TAKE 
    'minutes': '0ad6',  # U+2032 PRIME 
    'seconds': '0ad7',  # U+2033 DOUBLE PRIME 
    'latincross': '0ad9',  # U+271D LATIN CROSS 
    'hexagram': '0ada',
    'filledrectbullet': '0adb',  #(U+25AC BLACK RECTANGLE)
    'filledlefttribullet': '0adc',  #(U+25C0 BLACK LEFT-POINTING TRIANGLE)
    'filledrighttribullet': '0add',  #(U+25B6 BLACK RIGHT-POINTING TRIANGLE)
    'emfilledcircle': '0ade',  #(U+25CF BLACK CIRCLE)
    'emfilledrect': '0adf',  #(U+25AE BLACK VERTICAL RECTANGLE)
    'enopencircbullet': '0ae0',  #(U+25E6 WHITE BULLET)
    'enopensquarebullet': '0ae1',  #(U+25AB WHITE SMALL SQUARE)
    'openrectbullet': '0ae2',  #(U+25AD WHITE RECTANGLE)
    'opentribulletup': '0ae3',  #(U+25B3 WHITE UP-POINTING TRIANGLE)
    'opentribulletdown': '0ae4',  #(U+25BD WHITE DOWN-POINTING TRIANGLE)
    'openstar': '0ae5',  #(U+2606 WHITE STAR)
    'enfilledcircbullet': '0ae6',  #(U+2022 BULLET)
    'enfilledsqbullet': '0ae7',  #(U+25AA BLACK SMALL SQUARE)
    'filledtribulletup': '0ae8',  #(U+25B2 BLACK UP-POINTING TRIANGLE)
    'filledtribulletdown': '0ae9',  #(U+25BC BLACK DOWN-POINTING TRIANGLE)
    'leftpointer': '0aea',  #(U+261C WHITE LEFT POINTING INDEX)
    'rightpointer': '0aeb',  #(U+261E WHITE RIGHT POINTING INDEX)
    'club': '0aec',  # U+2663 BLACK CLUB SUIT 
    'diamond': '0aed',  # U+2666 BLACK DIAMOND SUIT 
    'heart': '0aee',  # U+2665 BLACK HEART SUIT 
    'maltesecross': '0af0',  # U+2720 MALTESE CROSS 
    'dagger': '0af1',  # U+2020 DAGGER 
    'doubledagger': '0af2',  # U+2021 DOUBLE DAGGER 
    'checkmark': '0af3',  # U+2713 CHECK MARK 
    'ballotcross': '0af4',  # U+2717 BALLOT X 
    'musicalsharp': '0af5',  # U+266F MUSIC SHARP SIGN 
    'musicalflat': '0af6',  # U+266D MUSIC FLAT SIGN 
    'malesymbol': '0af7',  # U+2642 MALE SIGN 
    'femalesymbol': '0af8',  # U+2640 FEMALE SIGN 
    'telephone': '0af9',  # U+260E BLACK TELEPHONE 
    'telephonerecorder': '0afa',  # U+2315 TELEPHONE RECORDER 
    'phonographcopyright': '0afb',  # U+2117 SOUND RECORDING COPYRIGHT 
    'caret': '0afc',  # U+2038 CARET 
    'singlelowquotemark': '0afd',  # U+201A SINGLE LOW-9 QUOTATION MARK 
    'doublelowquotemark': '0afe',  # U+201E DOUBLE LOW-9 QUOTATION MARK 
    'cursor': '0aff',

    # APL
    # Byte 3 =': '0b
    
    'leftcaret': '0ba3',  #(U+003C LESS-THAN SIGN)
    'rightcaret': '0ba6',  #(U+003E GREATER-THAN SIGN)
    'downcaret': '0ba8',  #(U+2228 LOGICAL OR)
    'upcaret': '0ba9',  #(U+2227 LOGICAL AND)
    'overbar': '0bc0',  #(U+00AF MACRON)
    'downtack': '0bc2',  # U+22A5 UP TACK 
    'upshoe': '0bc3',  #(U+2229 INTERSECTION)
    'downstile': '0bc4',  # U+230A LEFT FLOOR 
    'underbar': '0bc6',  #(U+005F LOW LINE)
    'jot': '0bca',  # U+2218 RING OPERATOR 
    'quad': '0bcc',  # U+2395 APL FUNCTIONAL SYMBOL QUAD 
    'uptack': '0bce',  # U+22A4 DOWN TACK 
    'circle': '0bcf',  # U+25CB WHITE CIRCLE 
    'upstile': '0bd3',  # U+2308 LEFT CEILING 
    'downshoe': '0bd6',  #(U+222A UNION)
    'rightshoe': '0bd8',  #(U+2283 SUPERSET OF)
    'leftshoe': '0bda',  #(U+2282 SUBSET OF)
    'lefttack': '0bdc',  # U+22A2 RIGHT TACK 
    'righttack': '0bfc',  # U+22A3 LEFT TACK 

    # Hebrew
    # Byte 3 =': '0c
    
    'hebrew_doublelowline': '0cdf',  # U+2017 DOUBLE LOW LINE 
    'hebrew_aleph': '0ce0',  # U+05D0 HEBREW LETTER ALEF 
    'hebrew_bet': '0ce1',  # U+05D1 HEBREW LETTER BET 
    'hebrew_beth': '0ce1',  # deprecated 
    'hebrew_gimel': '0ce2',  # U+05D2 HEBREW LETTER GIMEL 
    'hebrew_gimmel': '0ce2',  # deprecated 
    'hebrew_dalet': '0ce3',  # U+05D3 HEBREW LETTER DALET 
    'hebrew_daleth': '0ce3',  # deprecated 
    'hebrew_he': '0ce4',  # U+05D4 HEBREW LETTER HE 
    'hebrew_waw': '0ce5',  # U+05D5 HEBREW LETTER VAV 
    'hebrew_zain': '0ce6',  # U+05D6 HEBREW LETTER ZAYIN 
    'hebrew_zayin': '0ce6',  # deprecated 
    'hebrew_chet': '0ce7',  # U+05D7 HEBREW LETTER HET 
    'hebrew_het': '0ce7',  # deprecated 
    'hebrew_tet': '0ce8',  # U+05D8 HEBREW LETTER TET 
    'hebrew_teth': '0ce8',  # deprecated 
    'hebrew_yod': '0ce9',  # U+05D9 HEBREW LETTER YOD 
    'hebrew_finalkaph': '0cea',  # U+05DA HEBREW LETTER FINAL KAF 
    'hebrew_kaph': '0ceb',  # U+05DB HEBREW LETTER KAF 
    'hebrew_lamed': '0cec',  # U+05DC HEBREW LETTER LAMED 
    'hebrew_finalmem': '0ced',  # U+05DD HEBREW LETTER FINAL MEM 
    'hebrew_mem': '0cee',  # U+05DE HEBREW LETTER MEM 
    'hebrew_finalnun': '0cef',  # U+05DF HEBREW LETTER FINAL NUN 
    'hebrew_nun': '0cf0',  # U+05E0 HEBREW LETTER NUN 
    'hebrew_samech': '0cf1',  # U+05E1 HEBREW LETTER SAMEKH 
    'hebrew_samekh': '0cf1',  # deprecated 
    'hebrew_ayin': '0cf2',  # U+05E2 HEBREW LETTER AYIN 
    'hebrew_finalpe': '0cf3',  # U+05E3 HEBREW LETTER FINAL PE 
    'hebrew_pe': '0cf4',  # U+05E4 HEBREW LETTER PE 
    'hebrew_finalzade': '0cf5',  # U+05E5 HEBREW LETTER FINAL TSADI 
    'hebrew_finalzadi': '0cf5',  # deprecated 
    'hebrew_zade': '0cf6',  # U+05E6 HEBREW LETTER TSADI 
    'hebrew_zadi': '0cf6',  # deprecated 
    'hebrew_qoph': '0cf7',  # U+05E7 HEBREW LETTER QOF 
    'hebrew_kuf': '0cf7',  # deprecated 
    'hebrew_resh': '0cf8',  # U+05E8 HEBREW LETTER RESH 
    'hebrew_shin': '0cf9',  # U+05E9 HEBREW LETTER SHIN 
    'hebrew_taw': '0cfa',  # U+05EA HEBREW LETTER TAV 
    'hebrew_taf': '0cfa',  # deprecated 
    'Hebrew_switch': 'ff7e',  # Alias for mode_switch 

    # Thai
    # Byte 3 =': '0d
    
    'Thai_kokai': '0da1',  # U+0E01 THAI CHARACTER KO KAI 
    'Thai_khokhai': '0da2',  # U+0E02 THAI CHARACTER KHO KHAI 
    'Thai_khokhuat': '0da3',  # U+0E03 THAI CHARACTER KHO KHUAT 
    'Thai_khokhwai': '0da4',  # U+0E04 THAI CHARACTER KHO KHWAI 
    'Thai_khokhon': '0da5',  # U+0E05 THAI CHARACTER KHO KHON 
    'Thai_khorakhang': '0da6',  # U+0E06 THAI CHARACTER KHO RAKHANG 
    'Thai_ngongu': '0da7',  # U+0E07 THAI CHARACTER NGO NGU 
    'Thai_chochan': '0da8',  # U+0E08 THAI CHARACTER CHO CHAN 
    'Thai_choching': '0da9',  # U+0E09 THAI CHARACTER CHO CHING 
    'Thai_chochang': '0daa',  # U+0E0A THAI CHARACTER CHO CHANG 
    'Thai_soso': '0dab',  # U+0E0B THAI CHARACTER SO SO 
    'Thai_chochoe': '0dac',  # U+0E0C THAI CHARACTER CHO CHOE 
    'Thai_yoying': '0dad',  # U+0E0D THAI CHARACTER YO YING 
    'Thai_dochada': '0dae',  # U+0E0E THAI CHARACTER DO CHADA 
    'Thai_topatak': '0daf',  # U+0E0F THAI CHARACTER TO PATAK 
    'Thai_thothan': '0db0',  # U+0E10 THAI CHARACTER THO THAN 
    'Thai_thonangmontho': '0db1',  # U+0E11 THAI CHARACTER THO NANGMONTHO 
    'Thai_thophuthao': '0db2',  # U+0E12 THAI CHARACTER THO PHUTHAO 
    'Thai_nonen': '0db3',  # U+0E13 THAI CHARACTER NO NEN 
    'Thai_dodek': '0db4',  # U+0E14 THAI CHARACTER DO DEK 
    'Thai_totao': '0db5',  # U+0E15 THAI CHARACTER TO TAO 
    'Thai_thothung': '0db6',  # U+0E16 THAI CHARACTER THO THUNG 
    'Thai_thothahan': '0db7',  # U+0E17 THAI CHARACTER THO THAHAN 
    'Thai_thothong': '0db8',  # U+0E18 THAI CHARACTER THO THONG 
    'Thai_nonu': '0db9',  # U+0E19 THAI CHARACTER NO NU 
    'Thai_bobaimai': '0dba',  # U+0E1A THAI CHARACTER BO BAIMAI 
    'Thai_popla': '0dbb',  # U+0E1B THAI CHARACTER PO PLA 
    'Thai_phophung': '0dbc',  # U+0E1C THAI CHARACTER PHO PHUNG 
    'Thai_fofa': '0dbd',  # U+0E1D THAI CHARACTER FO FA 
    'Thai_phophan': '0dbe',  # U+0E1E THAI CHARACTER PHO PHAN 
    'Thai_fofan': '0dbf',  # U+0E1F THAI CHARACTER FO FAN 
    'Thai_phosamphao': '0dc0',  # U+0E20 THAI CHARACTER PHO SAMPHAO 
    'Thai_moma': '0dc1',  # U+0E21 THAI CHARACTER MO MA 
    'Thai_yoyak': '0dc2',  # U+0E22 THAI CHARACTER YO YAK 
    'Thai_rorua': '0dc3',  # U+0E23 THAI CHARACTER RO RUA 
    'Thai_ru': '0dc4',  # U+0E24 THAI CHARACTER RU 
    'Thai_loling': '0dc5',  # U+0E25 THAI CHARACTER LO LING 
    'Thai_lu': '0dc6',  # U+0E26 THAI CHARACTER LU 
    'Thai_wowaen': '0dc7',  # U+0E27 THAI CHARACTER WO WAEN 
    'Thai_sosala': '0dc8',  # U+0E28 THAI CHARACTER SO SALA 
    'Thai_sorusi': '0dc9',  # U+0E29 THAI CHARACTER SO RUSI 
    'Thai_sosua': '0dca',  # U+0E2A THAI CHARACTER SO SUA 
    'Thai_hohip': '0dcb',  # U+0E2B THAI CHARACTER HO HIP 
    'Thai_lochula': '0dcc',  # U+0E2C THAI CHARACTER LO CHULA 
    'Thai_oang': '0dcd',  # U+0E2D THAI CHARACTER O ANG 
    'Thai_honokhuk': '0dce',  # U+0E2E THAI CHARACTER HO NOKHUK 
    'Thai_paiyannoi': '0dcf',  # U+0E2F THAI CHARACTER PAIYANNOI 
    'Thai_saraa': '0dd0',  # U+0E30 THAI CHARACTER SARA A 
    'Thai_maihanakat': '0dd1',  # U+0E31 THAI CHARACTER MAI HAN-AKAT 
    'Thai_saraaa': '0dd2',  # U+0E32 THAI CHARACTER SARA AA 
    'Thai_saraam': '0dd3',  # U+0E33 THAI CHARACTER SARA AM 
    'Thai_sarai': '0dd4',  # U+0E34 THAI CHARACTER SARA I 
    'Thai_saraii': '0dd5',  # U+0E35 THAI CHARACTER SARA II 
    'Thai_saraue': '0dd6',  # U+0E36 THAI CHARACTER SARA UE 
    'Thai_sarauee': '0dd7',  # U+0E37 THAI CHARACTER SARA UEE 
    'Thai_sarau': '0dd8',  # U+0E38 THAI CHARACTER SARA U 
    'Thai_sarauu': '0dd9',  # U+0E39 THAI CHARACTER SARA UU 
    'Thai_phinthu': '0dda',  # U+0E3A THAI CHARACTER PHINTHU 
    'Thai_maihanakat_maitho': '0dde',
    'Thai_baht': '0ddf',  # U+0E3F THAI CURRENCY SYMBOL BAHT 
    'Thai_sarae': '0de0',  # U+0E40 THAI CHARACTER SARA E 
    'Thai_saraae': '0de1',  # U+0E41 THAI CHARACTER SARA AE 
    'Thai_sarao': '0de2',  # U+0E42 THAI CHARACTER SARA O 
    'Thai_saraaimaimuan': '0de3',  # U+0E43 THAI CHARACTER SARA AI MAIMUAN 
    'Thai_saraaimaimalai': '0de4',  # U+0E44 THAI CHARACTER SARA AI MAIMALAI 
    'Thai_lakkhangyao': '0de5',  # U+0E45 THAI CHARACTER LAKKHANGYAO 
    'Thai_maiyamok': '0de6',  # U+0E46 THAI CHARACTER MAIYAMOK 
    'Thai_maitaikhu': '0de7',  # U+0E47 THAI CHARACTER MAITAIKHU 
    'Thai_maiek': '0de8',  # U+0E48 THAI CHARACTER MAI EK 
    'Thai_maitho': '0de9',  # U+0E49 THAI CHARACTER MAI THO 
    'Thai_maitri': '0dea',  # U+0E4A THAI CHARACTER MAI TRI 
    'Thai_maichattawa': '0deb',  # U+0E4B THAI CHARACTER MAI CHATTAWA 
    'Thai_thanthakhat': '0dec',  # U+0E4C THAI CHARACTER THANTHAKHAT 
    'Thai_nikhahit': '0ded',  # U+0E4D THAI CHARACTER NIKHAHIT 
    'Thai_leksun': '0df0',  # U+0E50 THAI DIGIT ZERO 
    'Thai_leknung': '0df1',  # U+0E51 THAI DIGIT ONE 
    'Thai_leksong': '0df2',  # U+0E52 THAI DIGIT TWO 
    'Thai_leksam': '0df3',  # U+0E53 THAI DIGIT THREE 
    'Thai_leksi': '0df4',  # U+0E54 THAI DIGIT FOUR 
    'Thai_lekha': '0df5',  # U+0E55 THAI DIGIT FIVE 
    'Thai_lekhok': '0df6',  # U+0E56 THAI DIGIT SIX 
    'Thai_lekchet': '0df7',  # U+0E57 THAI DIGIT SEVEN 
    'Thai_lekpaet': '0df8',  # U+0E58 THAI DIGIT EIGHT 
    'Thai_lekkao': '0df9',  # U+0E59 THAI DIGIT NINE 

    # Korean
    # Byte 3 =': '0e

    'Hangul': 'ff31',  # Hangul start/stop(toggle) 
    'Hangul_Start': 'ff32',  # Hangul start 
    'Hangul_End': 'ff33',  # Hangul end, English start 
    'Hangul_Hanja': 'ff34',  # Start Hangul->Hanja Conversion 
    'Hangul_Jamo': 'ff35',  # Hangul Jamo mode 
    'Hangul_Romaja': 'ff36',  # Hangul Romaja mode 
    'Hangul_Codeinput': 'ff37',  # Hangul code input mode 
    'Hangul_Jeonja': 'ff38',  # Jeonja mode 
    'Hangul_Banja': 'ff39',  # Banja mode 
    'Hangul_PreHanja': 'ff3a',  # Pre Hanja conversion 
    'Hangul_PostHanja': 'ff3b',  # Post Hanja conversion 
    'Hangul_SingleCandidate': 'ff3c',  # Single candidate 
    'Hangul_MultipleCandidate': 'ff3d',  # Multiple candidate 
    'Hangul_PreviousCandidate': 'ff3e',  # Previous candidate 
    'Hangul_Special': 'ff3f',  # Special symbols 
    'Hangul_switch': 'ff7e',  # Alias for mode_switch 

    # Hangul Consonant Characters 
    'Hangul_Kiyeog': '0ea1',
    'Hangul_SsangKiyeog': '0ea2',
    'Hangul_KiyeogSios': '0ea3',
    'Hangul_Nieun': '0ea4',
    'Hangul_NieunJieuj': '0ea5',
    'Hangul_NieunHieuh': '0ea6',
    'Hangul_Dikeud': '0ea7',
    'Hangul_SsangDikeud': '0ea8',
    'Hangul_Rieul': '0ea9',
    'Hangul_RieulKiyeog': '0eaa',
    'Hangul_RieulMieum': '0eab',
    'Hangul_RieulPieub': '0eac',
    'Hangul_RieulSios': '0ead',
    'Hangul_RieulTieut': '0eae',
    'Hangul_RieulPhieuf': '0eaf',
    'Hangul_RieulHieuh': '0eb0',
    'Hangul_Mieum': '0eb1',
    'Hangul_Pieub': '0eb2',
    'Hangul_SsangPieub': '0eb3',
    'Hangul_PieubSios': '0eb4',
    'Hangul_Sios': '0eb5',
    'Hangul_SsangSios': '0eb6',
    'Hangul_Ieung': '0eb7',
    'Hangul_Jieuj': '0eb8',
    'Hangul_SsangJieuj': '0eb9',
    'Hangul_Cieuc': '0eba',
    'Hangul_Khieuq': '0ebb',
    'Hangul_Tieut': '0ebc',
    'Hangul_Phieuf': '0ebd',
    'Hangul_Hieuh': '0ebe',

    # Hangul Vowel Characters 
    'Hangul_A': '0ebf',
    'Hangul_AE': '0ec0',
    'Hangul_YA': '0ec1',
    'Hangul_YAE': '0ec2',
    'Hangul_EO': '0ec3',
    'Hangul_E': '0ec4',
    'Hangul_YEO': '0ec5',
    'Hangul_YE': '0ec6',
    'Hangul_O': '0ec7',
    'Hangul_WA': '0ec8',
    'Hangul_WAE': '0ec9',
    'Hangul_OE': '0eca',
    'Hangul_YO': '0ecb',
    'Hangul_U': '0ecc',
    'Hangul_WEO': '0ecd',
    'Hangul_WE': '0ece',
    'Hangul_WI': '0ecf',
    'Hangul_YU': '0ed0',
    'Hangul_EU': '0ed1',
    'Hangul_YI': '0ed2',
    'Hangul_I': '0ed3',

    # Hangul syllable-final (JongSeong) Characters 
    'Hangul_J_Kiyeog': '0ed4',
    'Hangul_J_SsangKiyeog': '0ed5',
    'Hangul_J_KiyeogSios': '0ed6',
    'Hangul_J_Nieun': '0ed7',
    'Hangul_J_NieunJieuj': '0ed8',
    'Hangul_J_NieunHieuh': '0ed9',
    'Hangul_J_Dikeud': '0eda',
    'Hangul_J_Rieul': '0edb',
    'Hangul_J_RieulKiyeog': '0edc',
    'Hangul_J_RieulMieum': '0edd',
    'Hangul_J_RieulPieub': '0ede',
    'Hangul_J_RieulSios': '0edf',
    'Hangul_J_RieulTieut': '0ee0',
    'Hangul_J_RieulPhieuf': '0ee1',
    'Hangul_J_RieulHieuh': '0ee2',
    'Hangul_J_Mieum': '0ee3',
    'Hangul_J_Pieub': '0ee4',
    'Hangul_J_PieubSios': '0ee5',
    'Hangul_J_Sios': '0ee6',
    'Hangul_J_SsangSios': '0ee7',
    'Hangul_J_Ieung': '0ee8',
    'Hangul_J_Jieuj': '0ee9',
    'Hangul_J_Cieuc': '0eea',
    'Hangul_J_Khieuq': '0eeb',
    'Hangul_J_Tieut': '0eec',
    'Hangul_J_Phieuf': '0eed',
    'Hangul_J_Hieuh': '0eee',

    # Ancient Hangul Consonant Characters 
    'Hangul_RieulYeorinHieuh': '0eef',
    'Hangul_SunkyeongeumMieum': '0ef0',
    'Hangul_SunkyeongeumPieub': '0ef1',
    'Hangul_PanSios': '0ef2',
    'Hangul_KkogjiDalrinIeung': '0ef3',
    'Hangul_SunkyeongeumPhieuf': '0ef4',
    'Hangul_YeorinHieuh': '0ef5',

    # Ancient Hangul Vowel Characters 
    'Hangul_AraeA': '0ef6',
    'Hangul_AraeAE': '0ef7',

    # Ancient Hangul syllable-final (JongSeong) Characters 
    'Hangul_J_PanSios': '0ef8',
    'Hangul_J_KkogjiDalrinIeung': '0ef9',
    'Hangul_J_YeorinHieuh': '0efa',

    # Korean currency symbol 
    'Korean_Won': '0eff',  #(U+20A9 WON SIGN)

    # Armenian
    
    'Armenian_ligature_ew': '1000587',  # U+0587 ARMENIAN SMALL LIGATURE ECH YIWN 
    'Armenian_full_stop': '1000589',  # U+0589 ARMENIAN FULL STOP 
    'Armenian_verjaket': '1000589',  # U+0589 ARMENIAN FULL STOP 
    'Armenian_separation_mark': '100055d',  # U+055D ARMENIAN COMMA 
    'Armenian_but': '100055d',  # U+055D ARMENIAN COMMA 
    'Armenian_hyphen': '100058a',  # U+058A ARMENIAN HYPHEN 
    'Armenian_yentamna': '100058a',  # U+058A ARMENIAN HYPHEN 
    'Armenian_exclam': '100055c',  # U+055C ARMENIAN EXCLAMATION MARK 
    'Armenian_amanak': '100055c',  # U+055C ARMENIAN EXCLAMATION MARK 
    'Armenian_accent': '100055b',  # U+055B ARMENIAN EMPHASIS MARK 
    'Armenian_shesht': '100055b',  # U+055B ARMENIAN EMPHASIS MARK 
    'Armenian_question': '100055e',  # U+055E ARMENIAN QUESTION MARK 
    'Armenian_paruyk': '100055e',  # U+055E ARMENIAN QUESTION MARK 
    'Armenian_AYB': '1000531',  # U+0531 ARMENIAN CAPITAL LETTER AYB 
    'Armenian_ayb': '1000561',  # U+0561 ARMENIAN SMALL LETTER AYB 
    'Armenian_BEN': '1000532',  # U+0532 ARMENIAN CAPITAL LETTER BEN 
    'Armenian_ben': '1000562',  # U+0562 ARMENIAN SMALL LETTER BEN 
    'Armenian_GIM': '1000533',  # U+0533 ARMENIAN CAPITAL LETTER GIM 
    'Armenian_gim': '1000563',  # U+0563 ARMENIAN SMALL LETTER GIM 
    'Armenian_DA': '1000534',  # U+0534 ARMENIAN CAPITAL LETTER DA 
    'Armenian_da': '1000564',  # U+0564 ARMENIAN SMALL LETTER DA 
    'Armenian_YECH': '1000535',  # U+0535 ARMENIAN CAPITAL LETTER ECH 
    'Armenian_yech': '1000565',  # U+0565 ARMENIAN SMALL LETTER ECH 
    'Armenian_ZA': '1000536',  # U+0536 ARMENIAN CAPITAL LETTER ZA 
    'Armenian_za': '1000566',  # U+0566 ARMENIAN SMALL LETTER ZA 
    'Armenian_E': '1000537',  # U+0537 ARMENIAN CAPITAL LETTER EH 
    'Armenian_e': '1000567',  # U+0567 ARMENIAN SMALL LETTER EH 
    'Armenian_AT': '1000538',  # U+0538 ARMENIAN CAPITAL LETTER ET 
    'Armenian_at': '1000568',  # U+0568 ARMENIAN SMALL LETTER ET 
    'Armenian_TO': '1000539',  # U+0539 ARMENIAN CAPITAL LETTER TO 
    'Armenian_to': '1000569',  # U+0569 ARMENIAN SMALL LETTER TO 
    'Armenian_ZHE': '100053a',  # U+053A ARMENIAN CAPITAL LETTER ZHE 
    'Armenian_zhe': '100056a',  # U+056A ARMENIAN SMALL LETTER ZHE 
    'Armenian_INI': '100053b',  # U+053B ARMENIAN CAPITAL LETTER INI 
    'Armenian_ini': '100056b',  # U+056B ARMENIAN SMALL LETTER INI 
    'Armenian_LYUN': '100053c',  # U+053C ARMENIAN CAPITAL LETTER LIWN 
    'Armenian_lyun': '100056c',  # U+056C ARMENIAN SMALL LETTER LIWN 
    'Armenian_KHE': '100053d',  # U+053D ARMENIAN CAPITAL LETTER XEH 
    'Armenian_khe': '100056d',  # U+056D ARMENIAN SMALL LETTER XEH 
    'Armenian_TSA': '100053e',  # U+053E ARMENIAN CAPITAL LETTER CA 
    'Armenian_tsa': '100056e',  # U+056E ARMENIAN SMALL LETTER CA 
    'Armenian_KEN': '100053f',  # U+053F ARMENIAN CAPITAL LETTER KEN 
    'Armenian_ken': '100056f',  # U+056F ARMENIAN SMALL LETTER KEN 
    'Armenian_HO': '1000540',  # U+0540 ARMENIAN CAPITAL LETTER HO 
    'Armenian_ho': '1000570',  # U+0570 ARMENIAN SMALL LETTER HO 
    'Armenian_DZA': '1000541',  # U+0541 ARMENIAN CAPITAL LETTER JA 
    'Armenian_dza': '1000571',  # U+0571 ARMENIAN SMALL LETTER JA 
    'Armenian_GHAT': '1000542',  # U+0542 ARMENIAN CAPITAL LETTER GHAD 
    'Armenian_ghat': '1000572',  # U+0572 ARMENIAN SMALL LETTER GHAD 
    'Armenian_TCHE': '1000543',  # U+0543 ARMENIAN CAPITAL LETTER CHEH 
    'Armenian_tche': '1000573',  # U+0573 ARMENIAN SMALL LETTER CHEH 
    'Armenian_MEN': '1000544',  # U+0544 ARMENIAN CAPITAL LETTER MEN 
    'Armenian_men': '1000574',  # U+0574 ARMENIAN SMALL LETTER MEN 
    'Armenian_HI': '1000545',  # U+0545 ARMENIAN CAPITAL LETTER YI 
    'Armenian_hi': '1000575',  # U+0575 ARMENIAN SMALL LETTER YI 
    'Armenian_NU': '1000546',  # U+0546 ARMENIAN CAPITAL LETTER NOW 
    'Armenian_nu': '1000576',  # U+0576 ARMENIAN SMALL LETTER NOW 
    'Armenian_SHA': '1000547',  # U+0547 ARMENIAN CAPITAL LETTER SHA 
    'Armenian_sha': '1000577',  # U+0577 ARMENIAN SMALL LETTER SHA 
    'Armenian_VO': '1000548',  # U+0548 ARMENIAN CAPITAL LETTER VO 
    'Armenian_vo': '1000578',  # U+0578 ARMENIAN SMALL LETTER VO 
    'Armenian_CHA': '1000549',  # U+0549 ARMENIAN CAPITAL LETTER CHA 
    'Armenian_cha': '1000579',  # U+0579 ARMENIAN SMALL LETTER CHA 
    'Armenian_PE': '100054a',  # U+054A ARMENIAN CAPITAL LETTER PEH 
    'Armenian_pe': '100057a',  # U+057A ARMENIAN SMALL LETTER PEH 
    'Armenian_JE': '100054b',  # U+054B ARMENIAN CAPITAL LETTER JHEH 
    'Armenian_je': '100057b',  # U+057B ARMENIAN SMALL LETTER JHEH 
    'Armenian_RA': '100054c',  # U+054C ARMENIAN CAPITAL LETTER RA 
    'Armenian_ra': '100057c',  # U+057C ARMENIAN SMALL LETTER RA 
    'Armenian_SE': '100054d',  # U+054D ARMENIAN CAPITAL LETTER SEH 
    'Armenian_se': '100057d',  # U+057D ARMENIAN SMALL LETTER SEH 
    'Armenian_VEV': '100054e',  # U+054E ARMENIAN CAPITAL LETTER VEW 
    'Armenian_vev': '100057e',  # U+057E ARMENIAN SMALL LETTER VEW 
    'Armenian_TYUN': '100054f',  # U+054F ARMENIAN CAPITAL LETTER TIWN 
    'Armenian_tyun': '100057f',  # U+057F ARMENIAN SMALL LETTER TIWN 
    'Armenian_RE': '1000550',  # U+0550 ARMENIAN CAPITAL LETTER REH 
    'Armenian_re': '1000580',  # U+0580 ARMENIAN SMALL LETTER REH 
    'Armenian_TSO': '1000551',  # U+0551 ARMENIAN CAPITAL LETTER CO 
    'Armenian_tso': '1000581',  # U+0581 ARMENIAN SMALL LETTER CO 
    'Armenian_VYUN': '1000552',  # U+0552 ARMENIAN CAPITAL LETTER YIWN 
    'Armenian_vyun': '1000582',  # U+0582 ARMENIAN SMALL LETTER YIWN 
    'Armenian_PYUR': '1000553',  # U+0553 ARMENIAN CAPITAL LETTER PIWR 
    'Armenian_pyur': '1000583',  # U+0583 ARMENIAN SMALL LETTER PIWR 
    'Armenian_KE': '1000554',  # U+0554 ARMENIAN CAPITAL LETTER KEH 
    'Armenian_ke': '1000584',  # U+0584 ARMENIAN SMALL LETTER KEH 
    'Armenian_O': '1000555',  # U+0555 ARMENIAN CAPITAL LETTER OH 
    'Armenian_o': '1000585',  # U+0585 ARMENIAN SMALL LETTER OH 
    'Armenian_FE': '1000556',  # U+0556 ARMENIAN CAPITAL LETTER FEH 
    'Armenian_fe': '1000586',  # U+0586 ARMENIAN SMALL LETTER FEH 
    'Armenian_apostrophe': '100055a',  # U+055A ARMENIAN APOSTROPHE 

    # Georgian
    
    'Georgian_an': '10010d0',  # U+10D0 GEORGIAN LETTER AN 
    'Georgian_ban': '10010d1',  # U+10D1 GEORGIAN LETTER BAN 
    'Georgian_gan': '10010d2',  # U+10D2 GEORGIAN LETTER GAN 
    'Georgian_don': '10010d3',  # U+10D3 GEORGIAN LETTER DON 
    'Georgian_en': '10010d4',  # U+10D4 GEORGIAN LETTER EN 
    'Georgian_vin': '10010d5',  # U+10D5 GEORGIAN LETTER VIN 
    'Georgian_zen': '10010d6',  # U+10D6 GEORGIAN LETTER ZEN 
    'Georgian_tan': '10010d7',  # U+10D7 GEORGIAN LETTER TAN 
    'Georgian_in': '10010d8',  # U+10D8 GEORGIAN LETTER IN 
    'Georgian_kan': '10010d9',  # U+10D9 GEORGIAN LETTER KAN 
    'Georgian_las': '10010da',  # U+10DA GEORGIAN LETTER LAS 
    'Georgian_man': '10010db',  # U+10DB GEORGIAN LETTER MAN 
    'Georgian_nar': '10010dc',  # U+10DC GEORGIAN LETTER NAR 
    'Georgian_on': '10010dd',  # U+10DD GEORGIAN LETTER ON 
    'Georgian_par': '10010de',  # U+10DE GEORGIAN LETTER PAR 
    'Georgian_zhar': '10010df',  # U+10DF GEORGIAN LETTER ZHAR 
    'Georgian_rae': '10010e0',  # U+10E0 GEORGIAN LETTER RAE 
    'Georgian_san': '10010e1',  # U+10E1 GEORGIAN LETTER SAN 
    'Georgian_tar': '10010e2',  # U+10E2 GEORGIAN LETTER TAR 
    'Georgian_un': '10010e3',  # U+10E3 GEORGIAN LETTER UN 
    'Georgian_phar': '10010e4',  # U+10E4 GEORGIAN LETTER PHAR 
    'Georgian_khar': '10010e5',  # U+10E5 GEORGIAN LETTER KHAR 
    'Georgian_ghan': '10010e6',  # U+10E6 GEORGIAN LETTER GHAN 
    'Georgian_qar': '10010e7',  # U+10E7 GEORGIAN LETTER QAR 
    'Georgian_shin': '10010e8',  # U+10E8 GEORGIAN LETTER SHIN 
    'Georgian_chin': '10010e9',  # U+10E9 GEORGIAN LETTER CHIN 
    'Georgian_can': '10010ea',  # U+10EA GEORGIAN LETTER CAN 
    'Georgian_jil': '10010eb',  # U+10EB GEORGIAN LETTER JIL 
    'Georgian_cil': '10010ec',  # U+10EC GEORGIAN LETTER CIL 
    'Georgian_char': '10010ed',  # U+10ED GEORGIAN LETTER CHAR 
    'Georgian_xan': '10010ee',  # U+10EE GEORGIAN LETTER XAN 
    'Georgian_jhan': '10010ef',  # U+10EF GEORGIAN LETTER JHAN 
    'Georgian_hae': '10010f0',  # U+10F0 GEORGIAN LETTER HAE 
    'Georgian_he': '10010f1',  # U+10F1 GEORGIAN LETTER HE 
    'Georgian_hie': '10010f2',  # U+10F2 GEORGIAN LETTER HIE 
    'Georgian_we': '10010f3',  # U+10F3 GEORGIAN LETTER WE 
    'Georgian_har': '10010f4',  # U+10F4 GEORGIAN LETTER HAR 
    'Georgian_hoe': '10010f5',  # U+10F5 GEORGIAN LETTER HOE 
    'Georgian_fi': '10010f6',  # U+10F6 GEORGIAN LETTER FI 

    # Azeri (and other Turkic or Caucasian languages)

    # latin 
    'Xabovedot': '1001e8a',  # U+1E8A LATIN CAPITAL LETTER X WITH DOT ABOVE 
    'Ibreve': '100012c',  # U+012C LATIN CAPITAL LETTER I WITH BREVE 
    'Zstroke': '10001b5',  # U+01B5 LATIN CAPITAL LETTER Z WITH STROKE 
    'Gcaron': '10001e6',  # U+01E6 LATIN CAPITAL LETTER G WITH CARON 
    'Ocaron': '10001d1',  # U+01D2 LATIN CAPITAL LETTER O WITH CARON 
    'Obarred': '100019f',  # U+019F LATIN CAPITAL LETTER O WITH MIDDLE TILDE 
    'xabovedot': '1001e8b',  # U+1E8B LATIN SMALL LETTER X WITH DOT ABOVE 
    'ibreve': '100012d',  # U+012D LATIN SMALL LETTER I WITH BREVE 
    'zstroke': '10001b6',  # U+01B6 LATIN SMALL LETTER Z WITH STROKE 
    'gcaron': '10001e7',  # U+01E7 LATIN SMALL LETTER G WITH CARON 
    'ocaron': '10001d2',  # U+01D2 LATIN SMALL LETTER O WITH CARON 
    'obarred': '1000275',  # U+0275 LATIN SMALL LETTER BARRED O 
    'SCHWA': '100018f',  # U+018F LATIN CAPITAL LETTER SCHWA 
    'schwa': '1000259',  # U+0259 LATIN SMALL LETTER SCHWA 
    # those are not really Caucasus 
    # For Inupiak 
    'Lbelowdot': '1001e36',  # U+1E36 LATIN CAPITAL LETTER L WITH DOT BELOW 
    'lbelowdot': '1001e37',  # U+1E37 LATIN SMALL LETTER L WITH DOT BELOW 

    # Vietnamese
    
    'Abelowdot': '1001ea0',  # U+1EA0 LATIN CAPITAL LETTER A WITH DOT BELOW 
    'abelowdot': '1001ea1',  # U+1EA1 LATIN SMALL LETTER A WITH DOT BELOW 
    'Ahook': '1001ea2',  # U+1EA2 LATIN CAPITAL LETTER A WITH HOOK ABOVE 
    'ahook': '1001ea3',  # U+1EA3 LATIN SMALL LETTER A WITH HOOK ABOVE 
    'Acircumflexacute': '1001ea4',  # U+1EA4 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND ACUTE 
    'acircumflexacute': '1001ea5',  # U+1EA5 LATIN SMALL LETTER A WITH CIRCUMFLEX AND ACUTE 
    'Acircumflexgrave': '1001ea6',  # U+1EA6 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND GRAVE 
    'acircumflexgrave': '1001ea7',  # U+1EA7 LATIN SMALL LETTER A WITH CIRCUMFLEX AND GRAVE 
    'Acircumflexhook': '1001ea8',  # U+1EA8 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND HOOK ABOVE 
    'acircumflexhook': '1001ea9',  # U+1EA9 LATIN SMALL LETTER A WITH CIRCUMFLEX AND HOOK ABOVE 
    'Acircumflextilde': '1001eaa',  # U+1EAA LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND TILDE 
    'acircumflextilde': '1001eab',  # U+1EAB LATIN SMALL LETTER A WITH CIRCUMFLEX AND TILDE 
    'Acircumflexbelowdot': '1001eac',  # U+1EAC LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND DOT BELOW 
    'acircumflexbelowdot': '1001ead',  # U+1EAD LATIN SMALL LETTER A WITH CIRCUMFLEX AND DOT BELOW 
    'Abreveacute': '1001eae',  # U+1EAE LATIN CAPITAL LETTER A WITH BREVE AND ACUTE 
    'abreveacute': '1001eaf',  # U+1EAF LATIN SMALL LETTER A WITH BREVE AND ACUTE 
    'Abrevegrave': '1001eb0',  # U+1EB0 LATIN CAPITAL LETTER A WITH BREVE AND GRAVE 
    'abrevegrave': '1001eb1',  # U+1EB1 LATIN SMALL LETTER A WITH BREVE AND GRAVE 
    'Abrevehook': '1001eb2',  # U+1EB2 LATIN CAPITAL LETTER A WITH BREVE AND HOOK ABOVE 
    'abrevehook': '1001eb3',  # U+1EB3 LATIN SMALL LETTER A WITH BREVE AND HOOK ABOVE 
    'Abrevetilde': '1001eb4',  # U+1EB4 LATIN CAPITAL LETTER A WITH BREVE AND TILDE 
    'abrevetilde': '1001eb5',  # U+1EB5 LATIN SMALL LETTER A WITH BREVE AND TILDE 
    'Abrevebelowdot': '1001eb6',  # U+1EB6 LATIN CAPITAL LETTER A WITH BREVE AND DOT BELOW 
    'abrevebelowdot': '1001eb7',  # U+1EB7 LATIN SMALL LETTER A WITH BREVE AND DOT BELOW 
    'Ebelowdot': '1001eb8',  # U+1EB8 LATIN CAPITAL LETTER E WITH DOT BELOW 
    'ebelowdot': '1001eb9',  # U+1EB9 LATIN SMALL LETTER E WITH DOT BELOW 
    'Ehook': '1001eba',  # U+1EBA LATIN CAPITAL LETTER E WITH HOOK ABOVE 
    'ehook': '1001ebb',  # U+1EBB LATIN SMALL LETTER E WITH HOOK ABOVE 
    'Etilde': '1001ebc',  # U+1EBC LATIN CAPITAL LETTER E WITH TILDE 
    'etilde': '1001ebd',  # U+1EBD LATIN SMALL LETTER E WITH TILDE 
    'Ecircumflexacute': '1001ebe',  # U+1EBE LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND ACUTE 
    'ecircumflexacute': '1001ebf',  # U+1EBF LATIN SMALL LETTER E WITH CIRCUMFLEX AND ACUTE 
    'Ecircumflexgrave': '1001ec0',  # U+1EC0 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND GRAVE 
    'ecircumflexgrave': '1001ec1',  # U+1EC1 LATIN SMALL LETTER E WITH CIRCUMFLEX AND GRAVE 
    'Ecircumflexhook': '1001ec2',  # U+1EC2 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND HOOK ABOVE 
    'ecircumflexhook': '1001ec3',  # U+1EC3 LATIN SMALL LETTER E WITH CIRCUMFLEX AND HOOK ABOVE 
    'Ecircumflextilde': '1001ec4',  # U+1EC4 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND TILDE 
    'ecircumflextilde': '1001ec5',  # U+1EC5 LATIN SMALL LETTER E WITH CIRCUMFLEX AND TILDE 
    'Ecircumflexbelowdot': '1001ec6',  # U+1EC6 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND DOT BELOW 
    'ecircumflexbelowdot': '1001ec7',  # U+1EC7 LATIN SMALL LETTER E WITH CIRCUMFLEX AND DOT BELOW 
    'Ihook': '1001ec8',  # U+1EC8 LATIN CAPITAL LETTER I WITH HOOK ABOVE 
    'ihook': '1001ec9',  # U+1EC9 LATIN SMALL LETTER I WITH HOOK ABOVE 
    'Ibelowdot': '1001eca',  # U+1ECA LATIN CAPITAL LETTER I WITH DOT BELOW 
    'ibelowdot': '1001ecb',  # U+1ECB LATIN SMALL LETTER I WITH DOT BELOW 
    'Obelowdot': '1001ecc',  # U+1ECC LATIN CAPITAL LETTER O WITH DOT BELOW 
    'obelowdot': '1001ecd',  # U+1ECD LATIN SMALL LETTER O WITH DOT BELOW 
    'Ohook': '1001ece',  # U+1ECE LATIN CAPITAL LETTER O WITH HOOK ABOVE 
    'ohook': '1001ecf',  # U+1ECF LATIN SMALL LETTER O WITH HOOK ABOVE 
    'Ocircumflexacute': '1001ed0',  # U+1ED0 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND ACUTE 
    'ocircumflexacute': '1001ed1',  # U+1ED1 LATIN SMALL LETTER O WITH CIRCUMFLEX AND ACUTE 
    'Ocircumflexgrave': '1001ed2',  # U+1ED2 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND GRAVE 
    'ocircumflexgrave': '1001ed3',  # U+1ED3 LATIN SMALL LETTER O WITH CIRCUMFLEX AND GRAVE 
    'Ocircumflexhook': '1001ed4',  # U+1ED4 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND HOOK ABOVE 
    'ocircumflexhook': '1001ed5',  # U+1ED5 LATIN SMALL LETTER O WITH CIRCUMFLEX AND HOOK ABOVE 
    'Ocircumflextilde': '1001ed6',  # U+1ED6 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND TILDE 
    'ocircumflextilde': '1001ed7',  # U+1ED7 LATIN SMALL LETTER O WITH CIRCUMFLEX AND TILDE 
    'Ocircumflexbelowdot': '1001ed8',  # U+1ED8 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND DOT BELOW 
    'ocircumflexbelowdot': '1001ed9',  # U+1ED9 LATIN SMALL LETTER O WITH CIRCUMFLEX AND DOT BELOW 
    'Ohornacute': '1001eda',  # U+1EDA LATIN CAPITAL LETTER O WITH HORN AND ACUTE 
    'ohornacute': '1001edb',  # U+1EDB LATIN SMALL LETTER O WITH HORN AND ACUTE 
    'Ohorngrave': '1001edc',  # U+1EDC LATIN CAPITAL LETTER O WITH HORN AND GRAVE 
    'ohorngrave': '1001edd',  # U+1EDD LATIN SMALL LETTER O WITH HORN AND GRAVE 
    'Ohornhook': '1001ede',  # U+1EDE LATIN CAPITAL LETTER O WITH HORN AND HOOK ABOVE 
    'ohornhook': '1001edf',  # U+1EDF LATIN SMALL LETTER O WITH HORN AND HOOK ABOVE 
    'Ohorntilde': '1001ee0',  # U+1EE0 LATIN CAPITAL LETTER O WITH HORN AND TILDE 
    'ohorntilde': '1001ee1',  # U+1EE1 LATIN SMALL LETTER O WITH HORN AND TILDE 
    'Ohornbelowdot': '1001ee2',  # U+1EE2 LATIN CAPITAL LETTER O WITH HORN AND DOT BELOW 
    'ohornbelowdot': '1001ee3',  # U+1EE3 LATIN SMALL LETTER O WITH HORN AND DOT BELOW 
    'Ubelowdot': '1001ee4',  # U+1EE4 LATIN CAPITAL LETTER U WITH DOT BELOW 
    'ubelowdot': '1001ee5',  # U+1EE5 LATIN SMALL LETTER U WITH DOT BELOW 
    'Uhook': '1001ee6',  # U+1EE6 LATIN CAPITAL LETTER U WITH HOOK ABOVE 
    'uhook': '1001ee7',  # U+1EE7 LATIN SMALL LETTER U WITH HOOK ABOVE 
    'Uhornacute': '1001ee8',  # U+1EE8 LATIN CAPITAL LETTER U WITH HORN AND ACUTE 
    'uhornacute': '1001ee9',  # U+1EE9 LATIN SMALL LETTER U WITH HORN AND ACUTE 
    'Uhorngrave': '1001eea',  # U+1EEA LATIN CAPITAL LETTER U WITH HORN AND GRAVE 
    'uhorngrave': '1001eeb',  # U+1EEB LATIN SMALL LETTER U WITH HORN AND GRAVE 
    'Uhornhook': '1001eec',  # U+1EEC LATIN CAPITAL LETTER U WITH HORN AND HOOK ABOVE 
    'uhornhook': '1001eed',  # U+1EED LATIN SMALL LETTER U WITH HORN AND HOOK ABOVE 
    'Uhorntilde': '1001eee',  # U+1EEE LATIN CAPITAL LETTER U WITH HORN AND TILDE 
    'uhorntilde': '1001eef',  # U+1EEF LATIN SMALL LETTER U WITH HORN AND TILDE 
    'Uhornbelowdot': '1001ef0',  # U+1EF0 LATIN CAPITAL LETTER U WITH HORN AND DOT BELOW 
    'uhornbelowdot': '1001ef1',  # U+1EF1 LATIN SMALL LETTER U WITH HORN AND DOT BELOW 
    'Ybelowdot': '1001ef4',  # U+1EF4 LATIN CAPITAL LETTER Y WITH DOT BELOW 
    'ybelowdot': '1001ef5',  # U+1EF5 LATIN SMALL LETTER Y WITH DOT BELOW 
    'Yhook': '1001ef6',  # U+1EF6 LATIN CAPITAL LETTER Y WITH HOOK ABOVE 
    'yhook': '1001ef7',  # U+1EF7 LATIN SMALL LETTER Y WITH HOOK ABOVE 
    'Ytilde': '1001ef8',  # U+1EF8 LATIN CAPITAL LETTER Y WITH TILDE 
    'ytilde': '1001ef9',  # U+1EF9 LATIN SMALL LETTER Y WITH TILDE 
    'Ohorn': '10001a0',  # U+01A0 LATIN CAPITAL LETTER O WITH HORN 
    'ohorn': '10001a1',  # U+01A1 LATIN SMALL LETTER O WITH HORN 
    'Uhorn': '10001af',  # U+01AF LATIN CAPITAL LETTER U WITH HORN 
    'uhorn': '10001b0',  # U+01B0 LATIN SMALL LETTER U WITH HORN 

    'EcuSign': '10020a0',  # U+20A0 EURO-CURRENCY SIGN 
    'ColonSign': '10020a1',  # U+20A1 COLON SIGN 
    'CruzeiroSign': '10020a2',  # U+20A2 CRUZEIRO SIGN 
    'FFrancSign': '10020a3',  # U+20A3 FRENCH FRANC SIGN 
    'LiraSign': '10020a4',  # U+20A4 LIRA SIGN 
    'MillSign': '10020a5',  # U+20A5 MILL SIGN 
    'NairaSign': '10020a6',  # U+20A6 NAIRA SIGN 
    'PesetaSign': '10020a7',  # U+20A7 PESETA SIGN 
    'RupeeSign': '10020a8',  # U+20A8 RUPEE SIGN 
    'WonSign': '10020a9',  # U+20A9 WON SIGN 
    'NewSheqelSign': '10020aa',  # U+20AA NEW SHEQEL SIGN 
    'DongSign': '10020ab',  # U+20AB DONG SIGN 
    'EuroSign': '20ac',  # U+20AC EURO SIGN 
}