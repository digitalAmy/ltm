import unittest
from ltm.run import Run, Step
from ltm.keysymmap import keysyms
import os

class TestRun(unittest.TestCase):

    def test_build_frames(self):
        run = Run(['me'], 30, 'test/output/rerecord_count')
        run.steps.append(Step(10, [keysyms['a']]))

        frames = run.build_frames()

        self.assertEqual(11, len(frames))
        self.assertEqual(keysyms['a'], frames[-1].keys[0])

    def test_build_input_config(self):
        artifacts = 'test/output/test_ltm/'
        os.makedirs(artifacts, exist_ok=True)
        with open('test/output/rerecord_count', 'w') as rerecord_count_file:
            rerecord_count_file.write('0')
        run = Run(['me'], 30, 'test/output/rerecord_count')

        input_config = run.build_input_config()

        self.assertEqual('0', input_config.rerecord_count)
        self.assertEqual('me', input_config.authors)
        self.assertEqual('30', input_config.framerate_num)

    def test_record(self):
        artifacts = 'test/output/test_ltm/'
        os.makedirs(artifacts, exist_ok=True)
        with open('test/output/rerecord_count', 'w') as rerecord_count_file:
            rerecord_count_file.write('0')
        run = Run(['me'], 30, 'test/output/rerecord_count')

        (_, input_config) = run.record()

        self.assertEqual('1', input_config.rerecord_count)
        self.assertEqual('me', input_config.authors)
        self.assertEqual('30', input_config.framerate_num)

