import unittest
from ltm.ltm_file import LtmFile
from ltm.inputs import Frame, MouseInput
from ltm.annotations import Annotations
from ltm.input_config import InputConfig
import os


class TestLtm(unittest.TestCase):

    def test(self):
        artifacts = 'test/output/test_ltm/'
        os.makedirs(artifacts, exist_ok=True)
        (os.remove(artifact) for artifact in os.listdir())

        frames = []
        frames.append(Frame(0, MouseInput()))
        frames.append(Frame(1, MouseInput()))
        frames.append(Frame(2, MouseInput()))
        frames.append(Frame(3, MouseInput()))
        frames.append(Frame(4, MouseInput()))

        LtmFile(InputConfig(), Annotations(''),
                frames).build_file(artifacts + 'test.ltm')
        self.assertTrue('test.ltm', os.listdir(artifacts))
