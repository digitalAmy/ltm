import unittest
from ltm.input_config import InputConfig, Timetrack


class TestInputConfig(unittest.TestCase):

    def test(self):
        self.maxDiff = None
        input_config = InputConfig()

        self.assertEqual(
            """[General]
authors=me
frame_count=0
framerate_den=1
framerate_num=30
game_name=game
initial_time_nsec=0
initial_time_sec=1
keyboard_support=true
libtas_major_version=1
libtas_minor_version=3
libtas_patch_version=4
mouse_support=false
nb_controllers=0
rerecord_count=0
savestate_frame_count=0

[mainthread_timetrack]
clock=-1
clock_gettime=-1
gettimeofday=-1
sdl_getperformancecounter=-1
sdl_getticks=-1
time=-1

[secondarythread_timetrack]
clock=-1
clock_gettime=-1
gettimeofday=-1
sdl_getperformancecounter=-1
sdl_getticks=-1
time=-1
""", input_config.fill_template())
