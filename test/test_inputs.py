import unittest
from ltm.inputs import Frame, MouseInput, lazy_mouse


class TestInputs(unittest.TestCase):

    def test_lazy_mouse(self):
        frames = []

        frames.append(Frame(0, MouseInput()))
        frames.append(Frame(1, MouseInput(10, 10)))
        frames.append(Frame(2, MouseInput()))
        frames.append(Frame(3, MouseInput(20, 30)))
        frames.append(Frame(4, MouseInput()))

        lazy_mouse(frames)

        self.assertEqual(
            (0, 0), (frames[0].mouse_input.mouse_x, frames[0].mouse_input.mouse_y))
        self.assertEqual(
            (10, 10), (frames[1].mouse_input.mouse_x, frames[1].mouse_input.mouse_y))
        self.assertEqual(
            (10, 10), (frames[2].mouse_input.mouse_x, frames[2].mouse_input.mouse_y))
        self.assertEqual(
            (20, 30), (frames[3].mouse_input.mouse_x, frames[3].mouse_input.mouse_y))
        self.assertEqual(
            (20, 30), (frames[4].mouse_input.mouse_x, frames[4].mouse_input.mouse_y))
